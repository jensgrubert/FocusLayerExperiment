/////// NUR DIESE VARIABLEN ÄNDERN START ///////

var maxNumTrials = 70.0; //320.0; // 256.0 // WICHTIG: zahl mit .0 angeben und sollte vielfaches von acht sein
// we assume a random number in the range of [0..1]
// the following variables are the max cummulative thresholds for certain events
var _maxChanceOdd = 0.5; // we have a 50% chance of showing an odd ring
var _maxChanceOddForeground = 0.333333333; // if we show an odd ring, we have a 50% chance of showing it in the background --> total chance of 25%

/*
// 24_Symbole
var _baseNameOneFLNoOdd = "/Matrizen/Testnullen/24_Symbole/one_focuslayer/no_odd/";
var _baseNameOneFLOdd = "/Matrizen/Testnullen/24_Symbole/one_focuslayer/odd/";

var _baseNameTwoFLProjNoOdd = "/Matrizen/Testnullen/24_Symbole/two_focuslayer/projection/no_odd/";
var _baseNameTwoFLProjOdd = "/Matrizen/Testnullen/24_Symbole/two_focuslayer/projection/odd/";

var _baseNameTwoFLHMDNoOdd = "/Matrizen/Testnullen/24_Symbole/two_focuslayer/hmd/no_odd/";
var _baseNameTwoFLHMDOdd = "/Matrizen/Testnullen/24_Symbole/two_focuslayer//hmd/odd/";
*/

/*
// 56_Symbole
var _baseNameOneFLNoOdd = "/Matrizen/Testnullen/56_Symbole/one_focuslayer/no_odd/";
var _baseNameOneFLOdd = "/Matrizen/Testnullen/56_Symbole/one_focuslayer/odd/";

var _baseNameTwoFLProjNoOdd = "/Matrizen/Testnullen/56_Symbole/two_focuslayer/projection/no_odd/";
var _baseNameTwoFLProjOdd = "/Matrizen/Testnullen/56_Symbole/two_focuslayer/projection/odd/";

var _baseNameTwoFLHMDNoOdd = "/Matrizen/Testnullen/56_Symbole/two_focuslayer/hmd/no_odd/";
var _baseNameTwoFLHMDOdd = "/Matrizen/Testnullen/56_Symbole/two_focuslayer//hmd/odd/";
*/

// 84_Symbole
var _baseNameOneFLNoOdd = "/Matrizen/Testnullen/84_Symbole/one_focuslayer/no_odd/";
var _baseNameOneFLOdd = "/Matrizen/Testnullen/84_Symbole/one_focuslayer/odd/";

var _baseNameTwoFLProjNoOdd = "/Matrizen/Testnullen/84_Symbole/two_focuslayer/projection/no_odd/";
var _baseNameTwoFLProjOdd = "/Matrizen/Testnullen/84_Symbole/two_focuslayer/projection/odd/";

var _baseNameTwoFLHMDNoOdd = "/Matrizen/Testnullen/84_Symbole/two_focuslayer/hmd/no_odd/";
var _baseNameTwoFLHMDOdd = "/Matrizen/Testnullen/84_Symbole/two_focuslayer//hmd/odd/";



/////// NUR DIESE VARIABLEN ÄNDERN ENDE ///////

/////// AB HIER NICHTS MEHR ÄNDERN ///////

var http = require("http")
var express = require('express')
var app = express()
var url = require('url')
var fs = require('fs')
//var conf = require('./config.json');


// web server
// http und WebSocket Modul laden
var server = require('http').  createServer()


//Server auf Port 4080 starten
var port = 4080;
server.listen(port);

  //, function() {console.log('Listening on ' 
//+ server.address().port) });


// Server-Anfragen akzeptieren
server.on('request', app);

//Verknüpfung mit HTML-Datei
app.use(express.static(__dirname + '/public'));

//komplette Matrix
app.get('/hmd1/', function (req, res) {      
	res.sendFile(__dirname + '/public/HMD_1FE.html');
});

//komplette Matrix
app.get('/hmd2/', function (req, res) {      
  res.sendFile(__dirname + '/public/HMD_2FE.html');
});

//Teil-Matrix
app.get('/projektion_hoch/', function (req, res) {
	//Datei index.html ausgeben
	res.sendFile(__dirname + '/public/Projektion_hoch.html');
});

//Teil-Matrix
app.get('/projektion_niedrig/', function (req, res) {
  //Datei index.html ausgeben
  res.sendFile(__dirname + '/public/Projektion_niedrig.html');
});

// Kontroll-Webseite
app.get('/control/', function (req, res) {
	//Datei index.html ausgeben
	res.sendFile(__dirname + '/public/control.html');
});

var _curCondition = 0; // should be "1" for 1 focus layer and "2" for two focus layers
var _curTrialStartTime = 0;
var _curTrialEndTime = 0;
var _curTrial = 0;
var _curForegroundDiffers = 0;

//var curNumOddForeground = 0;
var maxNumOddForeground = 8;

//var _curBGOrientation = "";
//var _curOROrientation = "";
//var _curORPosition = "";

var _curORCol = "";
var _curORRow = "";

var _curCorrectDecision = "";

// we want to make sure that every odd position in the periphery is exactly shown the same number of times as the other positions
//var maxNumOddPerBackgroundPosition = (maxNumTrials * _maxChanceOdd * (1.0 - _maxChanceOddForeground)) / 8;

//var maxNumOddPerBackgroundPosition = ((maxNumTrials * _maxChanceOdd * (1.0 - _maxChanceOddForeground)) / 6) + 1;
//var maxNumOddPerForegroundPosition = ((maxNumTrials * _maxChanceOdd * (_maxChanceOddForeground)) / 3) + 1;

//var maxNumNonOdd = maxNumTrials * (1-_maxChanceOdd);
//var _maxNumOdd = Math.floor(maxNumTrials * _maxChanceOdd);
//var _curNumOdd = 0;
//var curNumNonOdd = 0;
//console.log("maxNumOddPerBackgroundPosition: " + maxNumOddPerBackgroundPosition);
//console.log("maxNumOddPerForegroundPosition: " + maxNumOddPerForegroundPosition);

//var _curNumORBackgroundPositions = [0,0,0,0,0,0,0,0];
//var _curNumORBackgroundPositions = [0,0,0,0,0,0];
//var _curNumORBackgroundPositions = [0,0,0,0,0,0];
//var _curNumORForegroundPositions = [0,0,0];
/*
var _curNumOddPOL = 0; var _curNumOddPO = 0;
var _curNumOddPOR = 0; var _curNumOddPL = 0;
var _curNumOddPR = 0; var _curNumOddPUL = 0;
var _curNumOddPU = 0; var _curNumOddPUR = 0;
  */  
function nextTrial() {
  console.log("_curTrial: " + _curTrial);
   // _curBGOrientation = bgOrientations[Math.round(Math.random()*(bgOrientations.length-1))];
   // _curOROrientation = orOrientations[Math.round(Math.random()*(orOrientations.length-1))];

    // first decide if we will have an oddring or not
    var curOddRand = Math.random();
    if(curOddRand < _maxChanceOdd /*& _curNumOdd < _maxNumOdd*/) { // we will show an odd ring
        //_curNumOdd++;
        _curCorrectDecision = "difference";
        
        // make sure that the odd ring orientation actually differs from the background orientation
        /*var oddOrientationDiffersFromBG = false;    
        while(oddOrientationDiffersFromBG == false) {
            _curOROrientation =  orOrientations[Math.round(Math.random()*(orOrientations.length-1))];
            if(_curOROrientation != _curBGOrientation) {
                 oddOrientationDiffersFromBG = true;
            }
          }*/
        
        //if we show an odd ring, now decide if it will be in the background or foreground
         var curOddForegroundRand = Math.random();
        
         if(curOddForegroundRand < _maxChanceOddForeground) { // we will show an odd ring in the foreground
             // handle the one focus layer case - we only need to update the HMD client
             
             /*for(var i = 0; i = 1000; i++)
             {
                  _curORPosition = orForegroundPositions[Math.round(Math.random()*(orForegroundPositions.length-1))]; 
                 // check if the current periphery position is still allowed to be shown
                 var curORPositionIndex = orForegroundPositions.indexOf(_curORPosition); // yes
                 if(_curNumORForegroundPositions[curORPositionIndex] < maxNumOddPerForegroundPosition) {
                    _curNumORForegroundPositions[curORPositionIndex]++; 
                     break;
                 } // else keep drawing
             }*/

             _curORCol = _orForegroundCols[Math.round(Math.random()*(_orForegroundCols.length-1))]; 
             _curORRow = _orForegroundRows[Math.round(Math.random()*(_orForegroundRows.length-1))]; 

             if(_curCondition == "1") {    
                var imageNameHMD = _baseNameOneFLOdd + _curORCol + "_" + _curORRow + ".png";     
                var imageNameProj = "null";
                var data = '{"action": "nextTrial", "source": "server", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';
                wss.broadcast(data);
            } else if(_curCondition == "2") {    
                var imageNameHMD = _baseNameTwoFLHMDNoOdd  + "0.png";  //_curBGOrientation + ".png"; /// gleiche ausirchtung wie hintergrund HMD _baseNameTwoFLHMDOdd + _curORPosition + "_" + _curOROrientation + "_"+ _curBGOrientation + ".png";     
                //var imageNameProj = _baseNameTwoFLProj + _curBGOrientation + ".png"; 
                var imageNameProj = _baseNameTwoFLProjOdd + _curORCol + "_" + _curORRow + ".png";     
                var data = '{"action": "nextTrial", "source": "server", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';
                wss.broadcast(data);
            }

           /*  _curORPosition = "m";
             if(_curCondition == "1") {    
                  var imageNameHMD = _baseNameOneFLOdd + "m_"+ _curOROrientation + "_"+ _curBGOrientation + ".png";     
                  var imageNameProj = "null";
                  var data = '{"action": "nextTrial", "source": "server", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';
                  wss.broadcast(data);
             } else if(_curCondition == "2") {    
                  var imageNameHMD = _baseNameTwoFLHMDNoOdd+ _curBGOrientation + ".png";   // no odd ring in the periphery          
                  var imageNameProj = _baseNameTwoFLProj + _curOROrientation + ".png";   // odd ring in the projection
                  var data = '{"action": "nextTrial", "source": "server", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';
                  wss.broadcast(data);
             }*/
             
         } else {
             // we will show an odd ring in the periphery
    
            // var validORPosition = false;

            /* for(var i = 0; i = 1000; i++)
             {
                  _curORPosition = orBackgroundPositions[Math.round(Math.random()*(orBackgroundPositions.length-1))]; 
                 // check if the current periphery position is still allowed to be shown
                 var curORPositionIndex = orBackgroundPositions.indexOf(_curORPosition); // yes
                 if(_curNumORBackgroundPositions[curORPositionIndex] < maxNumOddPerBackgroundPosition) {
                    _curNumORBackgroundPositions[curORPositionIndex]++; 
                     break;
                 } // else keep drawing
             }*/

            _curORCol = _orBackgroundCols[Math.round(Math.random()*(_orBackgroundCols.length-1))]; 
             _curORRow = _orBackgroundRows[Math.round(Math.random()*(_orBackgroundRows.length-1))]; 

            if(_curCondition == "1") {    
                var imageNameHMD = _baseNameOneFLOdd + _curORCol + "_" + _curORRow  + ".png";     
                var imageNameProj = "null";
                var data = '{"action": "nextTrial", "source": "server", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';
                wss.broadcast(data);
            } else if(_curCondition == "2") {    
                var imageNameHMD = _baseNameTwoFLHMDOdd + _curORCol + "_" + _curORRow + ".png";     
                //var imageNameProj = _baseNameTwoFLProj + _curBGOrientation + ".png"; 
                var imageNameProj = _baseNameTwoFLProjNoOdd + "0.png";  //_curBGOrientation + ".png"; //kein unterschied _baseNameTwoFLProjOdd + _curORPosition + "_" + _curOROrientation + "_"+ _curBGOrientation + ".png";     
                var data = '{"action": "nextTrial", "source": "server", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';
                wss.broadcast(data);
            }

             /* while(validORPosition == false) {
                _curORPosition = orBackgroundPositions[Math.round(Math.random()*(orBackgroundPositions.length-1))]; 
                 // check if the current periphery position is still allowed to be shown
                 var curORPositionIndex = orBackgroundPositions.indexOf(_curORPosition); // yes
                 if(_curNumORBackgroundPositions[curORPositionIndex] < maxNumOddPerBackgroundPosition) {
                    _curNumORBackgroundPositions[curORPositionIndex]++; 
                     validORPosition = true;
                 } // else keep drawing
                 
             } // end while*/
             
              /*if(_curCondition == "1") {    
                  var imageNameHMD = _baseNameOneFLOdd + _curORPosition + "_" + _curOROrientation + "_"+ _curBGOrientation + ".png";     
                  var imageNameProj = "null";
                  var data = '{"action": "nextTrial", "source": "server", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';
                  wss.broadcast(data);
              } else if(_curCondition == "2") {    
                  var imageNameHMD = _baseNameTwoFLHMDOdd + _curORPosition + "_" + _curOROrientation + "_"+ _curBGOrientation + ".png";     
                  var imageNameProj = _baseNameTwoFLProj + _curBGOrientation + ".png"; 
                  var data = '{"action": "nextTrial", "source": "server", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';
                  wss.broadcast(data);
              }*/            
         }
         
            
        
    } else { // we will not show an odd ring
      _curORCol = -1;
      _curORRow = -1;
        _curCorrectDecision = "noDifference";
        // handle the one focus layer case - we only need to update the HMD client
         if(_curCondition == "1") {
            // choose a random bg orientation
             
             var imageNameHMD = _baseNameOneFLNoOdd + "0.png";             
             var imageNameProj = "null";
             var data = '{"action": "nextTrial", "source": "server", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';

             wss.broadcast(data);
             
         } else if (_curCondition == "2") {
             // choose a random bg orientation
           
             var imageNameHMD = _baseNameTwoFLHMDNoOdd +  "0.png";             
             var imageNameProj = _baseNameTwoFLProjNoOdd + "0.png";             
             var data = '{"action": "nextTrial", "source": "server", "fileHMD": "' + imageNameHMD + '",  "fileProj": "' + imageNameProj + '"}';
             wss.broadcast(data);             
         }
        
        
        
    }
  
    
   _curTrialStartTime = new Date().getTime();
}
// websocket
// https://github.com/websockets/ws
var WebSocketServer = require('ws').Server;
 
 wss = new WebSocketServer({ port: 4081 });
 //wss = new WebSocketServer(wsServerURL);


var trialRunning = false;

//var bgOrientations = ["ol", "o", "or", "r", "ur", "u", "ul", "l"];
//var bgOrientations = ["l","l"];
//var orOrientations = ["ol", "o", "or", "r", "ur", "u", "ul", "l"];

//FÜR VORTEST: 9 orBackgroundPositions
//var orBackgroundPositions = [1, 2, 3, 4, 5, 6, 7, 8, 9];

//FÜR HAUPTEST: 9 orBackgroundPositions
//
//var orBackgroundPositions = [1, 2, 3, 4, 5, 6, 7, 8];
//var orBackgroundPositions = ["ol", "o", "or", "l", "r", "ul", "u", "ur"];
//var orBackgroundPositions = ["ol", "or", "l", "r", "ul",  "ur"];
//var orForegroundPositions = ["o", "m", "u"];
//var orBackgroundPositions = ["ol", "or", "ul", "ur"];


/*
// 56_Symbole
var _orForegroundCols = [4, 5];
var _orForegroundRows = [1, 2, 3, 4, 5, 6, 7];

var _orBackgroundCols = [3, 6];
var _orBackgroundRows = [1, 2, 3, 4, 5, 6, 7];
*/


// 84_Symbole
var _orForegroundCols = [5, 6, 7, 8];
var _orForegroundRows = [1, 2, 3, 4, 5, 6, 7];

var _orBackgroundCols = [1, 2, 3, 4, 9, 10, 11, 12];
var _orBackgroundRows = [1, 2, 3, 4, 5, 6, 7];



wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
   // 
      
    var jo = JSON.parse(message);
            
    if(jo.source == "control") {
     
        
        if(jo.action == "startCondition" && trialRunning == false) {
           console.log("startCondition");   
            trialRunning = true;
           
           // create log file
            createLogFile(jo.id, jo.condition)
            console.log("_curFileName: " + _curFileName);
           
            //logToFile("trialNum;oddDecisionCorrect;duration;bgOrientation;orPosition;orOrientation;\n");
            logToFile("trialNum;oddDecisionCorrect;duration;orCol;orRow\n");


            createLogFileGoNoGo(jo.id, jo.condition);
            logToFileGoNoGo("trialNum;isGoImageDisplayed;decisionCorrect\n");            

            // choose background orientation, odd ring orientation, odd ring position
            /*bgOrientations.shuffle();
            orOrientations.shuffle();
            orBackgroundPositions.shuffle();
            */
            // set the current condition:
            _curCondition = jo.condition;
            nextTrial();
            _curTrial++;
            
       }
    }
      
    
    
      
    if(jo.source == "projection") {
        
       if(jo.action == "noDifference") {
           console.log("noDifference");   
           
           // inform control about user decision
           
           // log decision
           
           _curTrialEndTime = new Date().getTime();
           var duration = _curTrialEndTime - _curTrialStartTime;
           
           var decisionCorrect = 1;
           if(_curCorrectDecision == "noDifference") {
               decisionCorrect = 0;
               var data = '{"action": "beepRight", "source": "server"}';
               wss.broadcast(data);   
           } else {
               var data = '{"action": "beepError", "source": "server"}';
               wss.broadcast(data);   
           }
        
          //logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curBGOrientation + ";" + _curORPosition + ";" + _curOROrientation + "\n" );
          logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curORCol + ";" + _curORRow + "\n" );
           
           /*// show next image
           if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }*/
           var data = '{"action": "showIntermediateImage", "source": "server"}';
           wss.broadcast(data);   
       } // end if action noDifference
        else if(jo.action == "difference") {
           console.log("difference");   
           
           // inform control about user decision
           
           // log decision
           
           _curTrialEndTime = new Date().getTime();
           var duration = _curTrialEndTime - _curTrialStartTime;
           
            var decisionCorrect = 1;
           if(_curCorrectDecision == "difference") {
               decisionCorrect = 0;
               var data = '{"action": "beepRight", "source": "server"}';
               wss.broadcast(data);   
           } else {
               var data = '{"action": "beepError", "source": "server"}';
               wss.broadcast(data);   
           }

  
        
         // logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curBGOrientation + ";" + _curORPosition + ";" + _curOROrientation + "\n" );
           logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curORCol + ";" + _curORRow + "\n" );
           // show next image
          /* if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }*/
           var data = '{"action": "showIntermediateImage", "source": "server"}';
           wss.broadcast(data);   
       }
       else if(jo.action == "nextTrial") {
        logToFileGoNoGo(_curTrial + ";" + jo.goImageDisplayed + ";" + jo.decisionCorrect + "\n");
          // show next image
           if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }
       }
    }  
  });

//  ws.send('something');
});


// SERVER SENDING BROADCAST DATA
wss.broadcast = function broadcast(data) {
	wss.clients.forEach(function each(client) {
	client.send(data);
  });
};


// helper functions
// zufällige Anzeige der Matrizenbilder
Array.prototype.shuffle = function() {
    var input = this; 
    for (var i = input.length-1; i >=0; i--) {     
        var randomIndex = Math.floor(Math.random()*(i+1)); 
        var itemAtIndex = input[randomIndex]; 

        input[randomIndex] = input[i]; 
        input[i] = itemAtIndex;
    } return input;
} // end function


// logging
var _curFileName = "";
var _curFileNameGoNoGo = "";

function createLogFile(participant, condition) {
    _curFileName = "logs_haupttest/" + _curFileName + new Date().getTime() + "_" + participant + "_" + condition + ".csv";
}
function createLogFileGoNoGo(participant, condition) {
    _curFileNameGoNoGo = "logs_haupttest/" + _curFileNameGoNoGo + new Date().getTime() + "_" + participant + "_" + condition + "_gonogo.csv";
}
function logToFile(strToLog) {
 fs.appendFile (_curFileName, strToLog, function(err, fd) {
   if (err) {
       return console.error(err);
   }
  console.log("Appended successfully!");     
}); }

function logToFileGoNoGo(strToLog) {
 fs.appendFile (_curFileNameGoNoGo, strToLog, function(err, fd) {
   if (err) {
       return console.error(err);
   }
  //console.log("Appended successfully!");     
}); }

 

// Portnummer in die Konsole schreiben
console.log('Der Server läuft nun unter http://127.0.0.1:' + port + '/');



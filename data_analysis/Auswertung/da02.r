# data for individual participants

p02_stereo_1fe <- read.csv("logs/1471866423982_pp02_stereo_1fe.csv", header=TRUE, sep=";")
p02_stereo_2fe <- read.csv("logs/1471867468688_pp02_stereo_2fe.csv", header=TRUE, sep=";")

p02_stereo_1fe_dur <- p02_stereo_1fe$duration
p02_stereo_2fe_dur <- p02_stereo_2fe$duration



p02_stereo_1fe_decCor <- p02_stereo_1fe$oddDecisionCorrect
p02_stereo_2fe_decCor <- p02_stereo_2fe$oddDecisionCorrect

p02_numTrials <- length(p02_stereo_1fe_decCor)
p02_stereo_1fe_numDecCor <- length(which(p02_stereo_1fe_decCor == 0))
p02_stereo_2fe_numDecCor <- length(which(p02_stereo_2fe_decCor == 0))


p02_stereo_1fe_perDecFalse <-  (p02_numTrials - p02_stereo_1fe_numDecCor) / p02_numTrials
p02_stereo_2fe_perDecFalse <-  (p02_numTrials - p02_stereo_2fe_numDecCor) / p02_numTrials



# descriptive statistics

summary(p02_stereo_1fe_dur)
summary(p02_stereo_2fe_dur)



# vis
boxplot(p02_stereo_1fe_dur, p02_stereo_2fe_dur)

length(p02_stereo_2fe_dur)

t.test(p02_stereo_1fe_dur, p02_stereo_2fe_dur, paired=TRUE)
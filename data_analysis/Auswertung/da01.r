# data for individual participants
p01_stereo_1fe <- read.csv("logs/1471869677455_pp01_stereo_1fe.csv", header=TRUE, sep=";")
p01_stereo_2fe <- read.csv("logs/1471870999329_pp01_stereo_2fe.csv", header=TRUE, sep=";")

p01_stereo_1fe_dur <- p01_stereo_1fe$duration
p01_stereo_2fe_dur <- p01_stereo_2fe$duration


p01_stereo_1fe_decCor <- p01_stereo_1fe$oddDecisionCorrect
p01_stereo_2fe_decCor <- p01_stereo_2fe$oddDecisionCorrect


p01_numTrials <- length(p01_stereo_1fe_decCor)
p01_stereo_1fe_numDecCor <- length(which(p01_stereo_1fe_decCor == 0))
p01_stereo_2fe_numDecCor <- length(which(p01_stereo_2fe_decCor == 0))

p01_stereo_1fe_perDecFalse <-  (p01_numTrials - p01_stereo_1fe_numDecCor) / p01_numTrials
p01_stereo_2fe_perDecFalse <-  (p01_numTrials - p01_stereo_2fe_numDecCor) / p01_numTrials


# data for all participants
# for all data, spec. duration
all_stereo_1fe <- rbind(p01_stereo_1fe, p02_stereo_1fe)
all_stereo_2fe <- rbind(p01_stereo_2fe, p02_stereo_2fe)

# for overall error
all_numDecCor_1fe <- c(p01_stereo_1fe_numDecCor, p02_stereo_1fe_numDecCor)
all_numDecCor_2fe <- c(p01_stereo_2fe_numDecCor, p02_stereo_2fe_numDecCor)


# descriptive statistics
summary(p01_stereo_1fe_dur)
summary(p01_stereo_2fe_dur)


# vis
boxplot(p01_stereo_1fe_dur, p01_stereo_2fe_dur)

length(p01_stereo_2fe_dur)

t.test(p01_stereo_1fe_dur, p01_stereo_2fe_dur, paired=TRUE)


# data for individual participants
p01_stereo_1fe <- read.csv("logs/pp01_stereo_1fe.csv", header=TRUE, sep=";")
p01_stereo_2fe <- read.csv("logs/pp01_stereo_2fe.csv", header=TRUE, sep=";")

p02_stereo_1fe <- read.csv("logs/pp02_stereo_1fe.csv", header=TRUE, sep=";")
p02_stereo_2fe <- read.csv("logs/pp02_stereo_2fe.csv", header=TRUE, sep=";")

p03_stereo_1fe <- read.csv("logs/pp03_stereo_1fe.csv", header=TRUE, sep=";")
p03_stereo_2fe <- read.csv("logs/pp03_stereo_2fe.csv", header=TRUE, sep=";")

p04_stereo_1fe <- read.csv("logs/pp04_stereo_1fe.csv", header=TRUE, sep=";")
p04_stereo_2fe <- read.csv("logs/pp04_stereo_2fe.csv", header=TRUE, sep=";")

p05_stereo_1fe <- read.csv("logs/pp05_stereo_1fe.csv", header=TRUE, sep=";")
p05_stereo_2fe <- read.csv("logs/pp05_stereo_2fe.csv", header=TRUE, sep=";")

p06_stereo_1fe <- read.csv("logs/pp06_stereo_1fe.csv", header=TRUE, sep=";")
p06_stereo_2fe <- read.csv("logs/pp06_stereo_2fe.csv", header=TRUE, sep=";")

p07_stereo_1fe <- read.csv("logs/pp07_stereo_1fe.csv", header=TRUE, sep=";")
p07_stereo_2fe <- read.csv("logs/pp07_stereo_2fe.csv", header=TRUE, sep=";")

p08_stereo_1fe <- read.csv("logs/pp08_stereo_1fe.csv", header=TRUE, sep=";")
p08_stereo_2fe <- read.csv("logs/pp08_stereo_2fe.csv", header=TRUE, sep=";")

p09_stereo_1fe <- read.csv("logs/pp09_stereo_1fe.csv", header=TRUE, sep=";")
p09_stereo_2fe <- read.csv("logs/pp09_stereo_2fe.csv", header=TRUE, sep=";")

p10_stereo_1fe <- read.csv("logs/pp10_stereo_1fe.csv", header=TRUE, sep=";")
p10_stereo_2fe <- read.csv("logs/pp10_stereo_2fe.csv", header=TRUE, sep=";")

p11_stereo_1fe <- read.csv("logs/pp11_stereo_1fe.csv", header=TRUE, sep=";")
p11_stereo_2fe <- read.csv("logs/pp11_stereo_2fe.csv", header=TRUE, sep=";")

p12_stereo_1fe <- read.csv("logs/pp12_stereo_1fe.csv", header=TRUE, sep=";")
p12_stereo_2fe <- read.csv("logs/pp12_stereo_2fe.csv", header=TRUE, sep=";")

p13_stereo_1fe <- read.csv("logs/pp13_stereo_1fe.csv", header=TRUE, sep=";")
p13_stereo_2fe <- read.csv("logs/pp13_stereo_2fe.csv", header=TRUE, sep=";")

p14_stereo_1fe <- read.csv("logs/pp14_stereo_1fe.csv", header=TRUE, sep=";")
p14_stereo_2fe <- read.csv("logs/pp14_stereo_2fe.csv", header=TRUE, sep=";")

p15_stereo_1fe <- read.csv("logs/pp15_stereo_1fe.csv", header=TRUE, sep=";")
p15_stereo_2fe <- read.csv("logs/pp15_stereo_2fe.csv", header=TRUE, sep=";")

p16_stereo_1fe <- read.csv("logs/pp16_stereo_1fe.csv", header=TRUE, sep=";")
p16_stereo_2fe <- read.csv("logs/pp16_stereo_2fe.csv", header=TRUE, sep=";")

p17_stereo_1fe <- read.csv("logs/pp17_stereo_1fe.csv", header=TRUE, sep=";")
p17_stereo_2fe <- read.csv("logs/pp17_stereo_2fe.csv", header=TRUE, sep=";")

p18_stereo_1fe <- read.csv("logs/pp18_stereo_1fe.csv", header=TRUE, sep=";")
p18_stereo_2fe <- read.csv("logs/pp18_stereo_2fe.csv", header=TRUE, sep=";")

p19_stereo_1fe <- read.csv("logs/pp19_stereo_1fe.csv", header=TRUE, sep=";")
p19_stereo_2fe <- read.csv("logs/pp19_stereo_2fe.csv", header=TRUE, sep=";")

p20_stereo_1fe <- read.csv("logs/pp20_stereo_1fe.csv", header=TRUE, sep=";")
p20_stereo_2fe <- read.csv("logs/pp20_stereo_2fe.csv", header=TRUE, sep=";")

p21_stereo_1fe <- read.csv("logs/pp21_stereo_1fe.csv", header=TRUE, sep=";")
p21_stereo_2fe <- read.csv("logs/pp21_stereo_2fe.csv", header=TRUE, sep=";")

p22_stereo_1fe <- read.csv("logs/pp22_stereo_1fe.csv", header=TRUE, sep=";")
p22_stereo_2fe <- read.csv("logs/pp22_stereo_2fe.csv", header=TRUE, sep=";")

p23_stereo_1fe <- read.csv("logs/pp23_stereo_1fe.csv", header=TRUE, sep=";")
p23_stereo_2fe <- read.csv("logs/pp23_stereo_2fe.csv", header=TRUE, sep=";")

p24_stereo_1fe <- read.csv("logs/pp24_stereo_1fe.csv", header=TRUE, sep=";")
p24_stereo_2fe <- read.csv("logs/pp24_stereo_2fe.csv", header=TRUE, sep=";")


### .. until part 24

# error count per participant

numTrials <- 240
p01_stereo_1fe_numDecFalse <- length(which(p01_stereo_1fe$oddDecisionCorrect == 1))
p01_stereo_2fe_numDecFalse <- length(which(p01_stereo_2fe$oddDecisionCorrect == 1))

p01_stereo_2fe_numDecFalse_fg <- length(which(p01_stereo_2fe$oddDecisionCorrect == 1 & p01_stereo_1fe$orCol > 4 & p01_stereo_1fe$orCol < 9))
p01_stereo_2fe_numDecFalse_bg <- length(which(p01_stereo_2fe$oddDecisionCorrect == 1 & (p01_stereo_1fe$orCol <= 4 | p01_stereo_1fe$orCol >= 9)))
p01_stereo_2fe_numDecFalse_bgl <- length(which(p01_stereo_2fe$oddDecisionCorrect == 1 & p01_stereo_1fe$orCol <= 4))
p01_stereo_2fe_numDecFalse_bgr <- length(which(p01_stereo_2fe$oddDecisionCorrect == 1 & p01_stereo_1fe$orCol >= 9))

p01_stereo_1fe_numDecFalse_all <- length(which(p01_stereo_1fe$oddDecisionCorrect == 1 & p01_stereo_1fe$orCol >= 1 & p01_stereo_1fe$orCol <= 12))
p01_stereo_1fe_numDecFalse_left <- length(which(p01_stereo_1fe$oddDecisionCorrect == 1 & p01_stereo_1fe$orCol <= 4))
p01_stereo_1fe_numDecFalse_center <- length(which(p01_stereo_1fe$oddDecisionCorrect == 1 & p01_stereo_1fe$orCol > 4 & p01_stereo_1fe$orCol < 9))
p01_stereo_1fe_numDecFalse_right <- length(which(p01_stereo_1fe$oddDecisionCorrect == 1 & p01_stereo_1fe$orCol >= 9))

p02_stereo_1fe_numDecFalse <- length(which(p02_stereo_1fe$oddDecisionCorrect == 1))
p02_stereo_2fe_numDecFalse <- length(which(p02_stereo_2fe$oddDecisionCorrect == 1))

p02_stereo_2fe_numDecFalse_fg <- length(which(p02_stereo_2fe$oddDecisionCorrect == 1 & p02_stereo_1fe$orCol > 4 & p02_stereo_1fe$orCol < 9))
p02_stereo_2fe_numDecFalse_bg <- length(which(p02_stereo_2fe$oddDecisionCorrect == 1 & (p02_stereo_1fe$orCol <= 4 | p02_stereo_1fe$orCol >= 9)))
p02_stereo_2fe_numDecFalse_bgl <- length(which(p02_stereo_2fe$oddDecisionCorrect == 1 & p02_stereo_1fe$orCol <= 4))
p02_stereo_2fe_numDecFalse_bgr <- length(which(p02_stereo_2fe$oddDecisionCorrect == 1 & p02_stereo_1fe$orCol >= 9))

p02_stereo_1fe_numDecFalse_all <- length(which(p02_stereo_1fe$oddDecisionCorrect == 1 & p02_stereo_1fe$orCol >= 1 & p02_stereo_1fe$orCol <= 12))
p02_stereo_1fe_numDecFalse_left <- length(which(p02_stereo_1fe$oddDecisionCorrect == 1 & p02_stereo_1fe$orCol <= 4))
p02_stereo_1fe_numDecFalse_center <- length(which(p02_stereo_1fe$oddDecisionCorrect == 1 & p02_stereo_1fe$orCol > 4 & p02_stereo_1fe$orCol < 9))
p02_stereo_1fe_numDecFalse_right <- length(which(p02_stereo_1fe$oddDecisionCorrect == 1 & p02_stereo_1fe$orCol >= 9))

p03_stereo_1fe_numDecFalse <- length(which(p03_stereo_1fe$oddDecisionCorrect == 1))
p03_stereo_2fe_numDecFalse <- length(which(p03_stereo_2fe$oddDecisionCorrect == 1))

p03_stereo_2fe_numDecFalse_fg <- length(which(p03_stereo_2fe$oddDecisionCorrect == 1 & p03_stereo_1fe$orCol > 4 & p03_stereo_1fe$orCol < 9))
p03_stereo_2fe_numDecFalse_bg <- length(which(p03_stereo_2fe$oddDecisionCorrect == 1 & (p03_stereo_1fe$orCol <= 4 | p03_stereo_1fe$orCol >= 9)))
p03_stereo_2fe_numDecFalse_bgl <- length(which(p03_stereo_2fe$oddDecisionCorrect == 1 & p03_stereo_1fe$orCol <= 4))
p03_stereo_2fe_numDecFalse_bgr <- length(which(p03_stereo_2fe$oddDecisionCorrect == 1 & p03_stereo_1fe$orCol >= 9))

p03_stereo_1fe_numDecFalse_all <- length(which(p03_stereo_1fe$oddDecisionCorrect == 1 & p03_stereo_1fe$orCol >= 1 & p03_stereo_1fe$orCol <= 12))
p03_stereo_1fe_numDecFalse_left <- length(which(p03_stereo_1fe$oddDecisionCorrect == 1 & p03_stereo_1fe$orCol <= 4))
p03_stereo_1fe_numDecFalse_center <- length(which(p03_stereo_1fe$oddDecisionCorrect == 1 & p03_stereo_1fe$orCol > 4 & p03_stereo_1fe$orCol < 9))
p03_stereo_1fe_numDecFalse_right <- length(which(p03_stereo_1fe$oddDecisionCorrect == 1 & p03_stereo_1fe$orCol >= 9))

p04_stereo_1fe_numDecFalse <- length(which(p04_stereo_1fe$oddDecisionCorrect == 1))
p04_stereo_2fe_numDecFalse <- length(which(p04_stereo_2fe$oddDecisionCorrect == 1))

p04_stereo_2fe_numDecFalse_fg <- length(which(p04_stereo_2fe$oddDecisionCorrect == 1 & p04_stereo_1fe$orCol > 4 & p04_stereo_1fe$orCol < 9))
p04_stereo_2fe_numDecFalse_bg <- length(which(p04_stereo_2fe$oddDecisionCorrect == 1 & (p04_stereo_1fe$orCol <= 4 | p04_stereo_1fe$orCol >= 9)))
p04_stereo_2fe_numDecFalse_bgl <- length(which(p04_stereo_2fe$oddDecisionCorrect == 1 & p04_stereo_1fe$orCol <= 4))
p04_stereo_2fe_numDecFalse_bgr <- length(which(p04_stereo_2fe$oddDecisionCorrect == 1 & p04_stereo_1fe$orCol >= 9))

p04_stereo_1fe_numDecFalse_all <- length(which(p04_stereo_1fe$oddDecisionCorrect == 1 & p04_stereo_1fe$orCol >= 1 & p04_stereo_1fe$orCol <= 12))
p04_stereo_1fe_numDecFalse_left <- length(which(p04_stereo_1fe$oddDecisionCorrect == 1 & p04_stereo_1fe$orCol <= 4))
p04_stereo_1fe_numDecFalse_center <- length(which(p04_stereo_1fe$oddDecisionCorrect == 1 & p04_stereo_1fe$orCol > 4 & p04_stereo_1fe$orCol < 9))
p04_stereo_1fe_numDecFalse_right <- length(which(p04_stereo_1fe$oddDecisionCorrect == 1 & p04_stereo_1fe$orCol >= 9))

p05_stereo_1fe_numDecFalse <- length(which(p05_stereo_1fe$oddDecisionCorrect == 1))
p05_stereo_2fe_numDecFalse <- length(which(p05_stereo_2fe$oddDecisionCorrect == 1))

p05_stereo_2fe_numDecFalse_fg <- length(which(p05_stereo_2fe$oddDecisionCorrect == 1 & p05_stereo_1fe$orCol > 4 & p05_stereo_1fe$orCol < 9))
p05_stereo_2fe_numDecFalse_bg <- length(which(p05_stereo_2fe$oddDecisionCorrect == 1 & (p05_stereo_1fe$orCol <= 4 | p05_stereo_1fe$orCol >= 9)))
p05_stereo_2fe_numDecFalse_bgl <- length(which(p05_stereo_2fe$oddDecisionCorrect == 1 & p05_stereo_1fe$orCol <= 4))
p05_stereo_2fe_numDecFalse_bgr <- length(which(p05_stereo_2fe$oddDecisionCorrect == 1 & p05_stereo_1fe$orCol >= 9))

p05_stereo_1fe_numDecFalse_all <- length(which(p05_stereo_1fe$oddDecisionCorrect == 1 & p05_stereo_1fe$orCol >= 1 & p05_stereo_1fe$orCol <= 12))
p05_stereo_1fe_numDecFalse_left <- length(which(p05_stereo_1fe$oddDecisionCorrect == 1 & p05_stereo_1fe$orCol <= 4))
p05_stereo_1fe_numDecFalse_center <- length(which(p05_stereo_1fe$oddDecisionCorrect == 1 & p05_stereo_1fe$orCol > 4 & p05_stereo_1fe$orCol < 9))
p05_stereo_1fe_numDecFalse_right <- length(which(p05_stereo_1fe$oddDecisionCorrect == 1 & p05_stereo_1fe$orCol >= 9))

p06_stereo_1fe_numDecFalse <- length(which(p06_stereo_1fe$oddDecisionCorrect == 1))
p06_stereo_2fe_numDecFalse <- length(which(p06_stereo_2fe$oddDecisionCorrect == 1))

p06_stereo_2fe_numDecFalse_fg <- length(which(p06_stereo_2fe$oddDecisionCorrect == 1 & p06_stereo_1fe$orCol > 4 & p06_stereo_1fe$orCol < 9))
p06_stereo_2fe_numDecFalse_bg <- length(which(p06_stereo_2fe$oddDecisionCorrect == 1 & (p06_stereo_1fe$orCol <= 4 | p06_stereo_1fe$orCol >= 9)))
p06_stereo_2fe_numDecFalse_bgl <- length(which(p06_stereo_2fe$oddDecisionCorrect == 1 & p06_stereo_1fe$orCol <= 4))
p06_stereo_2fe_numDecFalse_bgr <- length(which(p06_stereo_2fe$oddDecisionCorrect == 1 & p06_stereo_1fe$orCol >= 9))

p06_stereo_1fe_numDecFalse_all <- length(which(p06_stereo_1fe$oddDecisionCorrect == 1 & p06_stereo_1fe$orCol >= 1 & p06_stereo_1fe$orCol <= 12))
p06_stereo_1fe_numDecFalse_left <- length(which(p06_stereo_1fe$oddDecisionCorrect == 1 & p06_stereo_1fe$orCol <= 4))
p06_stereo_1fe_numDecFalse_center <- length(which(p06_stereo_1fe$oddDecisionCorrect == 1 & p06_stereo_1fe$orCol > 4 & p06_stereo_1fe$orCol < 9))
p06_stereo_1fe_numDecFalse_right <- length(which(p06_stereo_1fe$oddDecisionCorrect == 1 & p06_stereo_1fe$orCol >= 9))

p07_stereo_1fe_numDecFalse <- length(which(p07_stereo_1fe$oddDecisionCorrect == 1))
p07_stereo_2fe_numDecFalse <- length(which(p07_stereo_2fe$oddDecisionCorrect == 1))

p07_stereo_2fe_numDecFalse_fg <- length(which(p07_stereo_2fe$oddDecisionCorrect == 1 & p07_stereo_1fe$orCol > 4 & p07_stereo_1fe$orCol < 9))
p07_stereo_2fe_numDecFalse_bg <- length(which(p07_stereo_2fe$oddDecisionCorrect == 1 & (p07_stereo_1fe$orCol <= 4 | p07_stereo_1fe$orCol >= 9)))
p07_stereo_2fe_numDecFalse_bgl <- length(which(p07_stereo_2fe$oddDecisionCorrect == 1 & p07_stereo_1fe$orCol <= 4))
p07_stereo_2fe_numDecFalse_bgr <- length(which(p07_stereo_2fe$oddDecisionCorrect == 1 & p07_stereo_1fe$orCol >= 9))

p07_stereo_1fe_numDecFalse_all <- length(which(p07_stereo_1fe$oddDecisionCorrect == 1 & p07_stereo_1fe$orCol >= 1 & p07_stereo_1fe$orCol <= 12))
p07_stereo_1fe_numDecFalse_left <- length(which(p07_stereo_1fe$oddDecisionCorrect == 1 & p07_stereo_1fe$orCol <= 4))
p07_stereo_1fe_numDecFalse_center <- length(which(p07_stereo_1fe$oddDecisionCorrect == 1 & p07_stereo_1fe$orCol > 4 & p07_stereo_1fe$orCol < 9))
p07_stereo_1fe_numDecFalse_right <- length(which(p07_stereo_1fe$oddDecisionCorrect == 1 & p07_stereo_1fe$orCol >= 9))

p08_stereo_1fe_numDecFalse <- length(which(p08_stereo_1fe$oddDecisionCorrect == 1))
p08_stereo_2fe_numDecFalse <- length(which(p08_stereo_2fe$oddDecisionCorrect == 1))

p08_stereo_2fe_numDecFalse_fg <- length(which(p08_stereo_2fe$oddDecisionCorrect == 1 & p08_stereo_1fe$orCol > 4 & p08_stereo_1fe$orCol < 9))
p08_stereo_2fe_numDecFalse_bg <- length(which(p08_stereo_2fe$oddDecisionCorrect == 1 & (p08_stereo_1fe$orCol <= 4 | p08_stereo_1fe$orCol >= 9)))
p08_stereo_2fe_numDecFalse_bgl <- length(which(p08_stereo_2fe$oddDecisionCorrect == 1 & p08_stereo_1fe$orCol <= 4))
p08_stereo_2fe_numDecFalse_bgr <- length(which(p08_stereo_2fe$oddDecisionCorrect == 1 & p08_stereo_1fe$orCol >= 9))

p08_stereo_1fe_numDecFalse_all <- length(which(p08_stereo_1fe$oddDecisionCorrect == 1 & p08_stereo_1fe$orCol >= 1 & p08_stereo_1fe$orCol <= 12))
p08_stereo_1fe_numDecFalse_left <- length(which(p08_stereo_1fe$oddDecisionCorrect == 1 & p08_stereo_1fe$orCol <= 4))
p08_stereo_1fe_numDecFalse_center <- length(which(p08_stereo_1fe$oddDecisionCorrect == 1 & p08_stereo_1fe$orCol > 4 & p08_stereo_1fe$orCol < 9))
p08_stereo_1fe_numDecFalse_right <- length(which(p08_stereo_1fe$oddDecisionCorrect == 1 & p08_stereo_1fe$orCol >= 9))

p09_stereo_1fe_numDecFalse <- length(which(p09_stereo_1fe$oddDecisionCorrect == 1))
p09_stereo_2fe_numDecFalse <- length(which(p09_stereo_2fe$oddDecisionCorrect == 1))

p09_stereo_2fe_numDecFalse_fg <- length(which(p09_stereo_2fe$oddDecisionCorrect == 1 & p09_stereo_1fe$orCol > 4 & p09_stereo_1fe$orCol < 9))
p09_stereo_2fe_numDecFalse_bg <- length(which(p09_stereo_2fe$oddDecisionCorrect == 1 & (p09_stereo_1fe$orCol <= 4 | p09_stereo_1fe$orCol >= 9)))
p09_stereo_2fe_numDecFalse_bgl <- length(which(p09_stereo_2fe$oddDecisionCorrect == 1 & p09_stereo_1fe$orCol <= 4))
p09_stereo_2fe_numDecFalse_bgr <- length(which(p09_stereo_2fe$oddDecisionCorrect == 1 & p09_stereo_1fe$orCol >= 9))

p09_stereo_1fe_numDecFalse_all <- length(which(p09_stereo_1fe$oddDecisionCorrect == 1 & p09_stereo_1fe$orCol >= 1 & p09_stereo_1fe$orCol <= 12))
p09_stereo_1fe_numDecFalse_left <- length(which(p09_stereo_1fe$oddDecisionCorrect == 1 & p09_stereo_1fe$orCol <= 4))
p09_stereo_1fe_numDecFalse_center <- length(which(p09_stereo_1fe$oddDecisionCorrect == 1 & p09_stereo_1fe$orCol > 4 & p09_stereo_1fe$orCol < 9))
p09_stereo_1fe_numDecFalse_right <- length(which(p09_stereo_1fe$oddDecisionCorrect == 1 & p09_stereo_1fe$orCol >= 9))

p10_stereo_1fe_numDecFalse <- length(which(p10_stereo_1fe$oddDecisionCorrect == 1))
p10_stereo_2fe_numDecFalse <- length(which(p10_stereo_2fe$oddDecisionCorrect == 1))

p10_stereo_2fe_numDecFalse_fg <- length(which(p10_stereo_2fe$oddDecisionCorrect == 1 & p10_stereo_1fe$orCol > 4 & p10_stereo_1fe$orCol < 9))
p10_stereo_2fe_numDecFalse_bg <- length(which(p10_stereo_2fe$oddDecisionCorrect == 1 & (p10_stereo_1fe$orCol <= 4 | p10_stereo_1fe$orCol >= 9)))
p10_stereo_2fe_numDecFalse_bgl <- length(which(p10_stereo_2fe$oddDecisionCorrect == 1 & p10_stereo_1fe$orCol <= 4))
p10_stereo_2fe_numDecFalse_bgr <- length(which(p10_stereo_2fe$oddDecisionCorrect == 1 & p10_stereo_1fe$orCol >= 9))

p10_stereo_1fe_numDecFalse_all <- length(which(p10_stereo_1fe$oddDecisionCorrect == 1 & p10_stereo_1fe$orCol >= 1 & p10_stereo_1fe$orCol <= 12))
p10_stereo_1fe_numDecFalse_left <- length(which(p10_stereo_1fe$oddDecisionCorrect == 1 & p10_stereo_1fe$orCol <= 4))
p10_stereo_1fe_numDecFalse_center <- length(which(p10_stereo_1fe$oddDecisionCorrect == 1 & p10_stereo_1fe$orCol > 4 & p10_stereo_1fe$orCol < 9))
p10_stereo_1fe_numDecFalse_right <- length(which(p10_stereo_1fe$oddDecisionCorrect == 1 & p10_stereo_1fe$orCol >= 9))

p11_stereo_1fe_numDecFalse <- length(which(p11_stereo_1fe$oddDecisionCorrect == 1))
p11_stereo_2fe_numDecFalse <- length(which(p11_stereo_2fe$oddDecisionCorrect == 1))

p11_stereo_2fe_numDecFalse_fg <- length(which(p11_stereo_2fe$oddDecisionCorrect == 1 & p11_stereo_1fe$orCol > 4 & p11_stereo_1fe$orCol < 9))
p11_stereo_2fe_numDecFalse_bg <- length(which(p11_stereo_2fe$oddDecisionCorrect == 1 & (p11_stereo_1fe$orCol <= 4 | p11_stereo_1fe$orCol >= 9)))
p11_stereo_2fe_numDecFalse_bgl <- length(which(p11_stereo_2fe$oddDecisionCorrect == 1 & p11_stereo_1fe$orCol <= 4))
p11_stereo_2fe_numDecFalse_bgr <- length(which(p11_stereo_2fe$oddDecisionCorrect == 1 & p11_stereo_1fe$orCol >= 9))

p11_stereo_1fe_numDecFalse_all <- length(which(p11_stereo_1fe$oddDecisionCorrect == 1 & p11_stereo_1fe$orCol >= 1 & p11_stereo_1fe$orCol <= 12))
p11_stereo_1fe_numDecFalse_left <- length(which(p11_stereo_1fe$oddDecisionCorrect == 1 & p11_stereo_1fe$orCol <= 4))
p11_stereo_1fe_numDecFalse_center <- length(which(p11_stereo_1fe$oddDecisionCorrect == 1 & p11_stereo_1fe$orCol > 4 & p11_stereo_1fe$orCol < 9))
p11_stereo_1fe_numDecFalse_right <- length(which(p11_stereo_1fe$oddDecisionCorrect == 1 & p11_stereo_1fe$orCol >= 9))

p12_stereo_1fe_numDecFalse <- length(which(p12_stereo_1fe$oddDecisionCorrect == 1))
p12_stereo_2fe_numDecFalse <- length(which(p12_stereo_2fe$oddDecisionCorrect == 1))

p12_stereo_2fe_numDecFalse_fg <- length(which(p12_stereo_2fe$oddDecisionCorrect == 1 & p12_stereo_1fe$orCol > 4 & p12_stereo_1fe$orCol < 9))
p12_stereo_2fe_numDecFalse_bg <- length(which(p12_stereo_2fe$oddDecisionCorrect == 1 & (p12_stereo_1fe$orCol <= 4 | p12_stereo_1fe$orCol >= 9)))
p12_stereo_2fe_numDecFalse_bgl <- length(which(p12_stereo_2fe$oddDecisionCorrect == 1 & p12_stereo_1fe$orCol <= 4))
p12_stereo_2fe_numDecFalse_bgr <- length(which(p12_stereo_2fe$oddDecisionCorrect == 1 & p12_stereo_1fe$orCol >= 9))

p12_stereo_1fe_numDecFalse_all <- length(which(p12_stereo_1fe$oddDecisionCorrect == 1 & p12_stereo_1fe$orCol >= 1 & p12_stereo_1fe$orCol <= 12))
p12_stereo_1fe_numDecFalse_left <- length(which(p12_stereo_1fe$oddDecisionCorrect == 1 & p12_stereo_1fe$orCol <= 4))
p12_stereo_1fe_numDecFalse_center <- length(which(p12_stereo_1fe$oddDecisionCorrect == 1 & p12_stereo_1fe$orCol > 4 & p12_stereo_1fe$orCol < 9))
p12_stereo_1fe_numDecFalse_right <- length(which(p12_stereo_1fe$oddDecisionCorrect == 1 & p12_stereo_1fe$orCol >= 9))

p13_stereo_1fe_numDecFalse <- length(which(p13_stereo_1fe$oddDecisionCorrect == 1))
p13_stereo_2fe_numDecFalse <- length(which(p13_stereo_2fe$oddDecisionCorrect == 1))

p13_stereo_2fe_numDecFalse_fg <- length(which(p13_stereo_2fe$oddDecisionCorrect == 1 & p13_stereo_1fe$orCol > 4 & p13_stereo_1fe$orCol < 9))
p13_stereo_2fe_numDecFalse_bg <- length(which(p13_stereo_2fe$oddDecisionCorrect == 1 & (p13_stereo_1fe$orCol <= 4 | p13_stereo_1fe$orCol >= 9)))
p13_stereo_2fe_numDecFalse_bgl <- length(which(p13_stereo_2fe$oddDecisionCorrect == 1 & p13_stereo_1fe$orCol <= 4))
p13_stereo_2fe_numDecFalse_bgr <- length(which(p13_stereo_2fe$oddDecisionCorrect == 1 & p13_stereo_1fe$orCol >= 9))

p13_stereo_1fe_numDecFalse_all <- length(which(p13_stereo_1fe$oddDecisionCorrect == 1 & p13_stereo_1fe$orCol >= 1 & p13_stereo_1fe$orCol <= 12))
p13_stereo_1fe_numDecFalse_left <- length(which(p13_stereo_1fe$oddDecisionCorrect == 1 & p13_stereo_1fe$orCol <= 4))
p13_stereo_1fe_numDecFalse_center <- length(which(p13_stereo_1fe$oddDecisionCorrect == 1 & p13_stereo_1fe$orCol > 4 & p13_stereo_1fe$orCol < 9))
p13_stereo_1fe_numDecFalse_right <- length(which(p13_stereo_1fe$oddDecisionCorrect == 1 & p13_stereo_1fe$orCol >= 9))

p14_stereo_1fe_numDecFalse <- length(which(p14_stereo_1fe$oddDecisionCorrect == 1))
p14_stereo_2fe_numDecFalse <- length(which(p14_stereo_2fe$oddDecisionCorrect == 1))

p14_stereo_2fe_numDecFalse_fg <- length(which(p14_stereo_2fe$oddDecisionCorrect == 1 & p14_stereo_1fe$orCol > 4 & p14_stereo_1fe$orCol < 9))
p14_stereo_2fe_numDecFalse_bg <- length(which(p14_stereo_2fe$oddDecisionCorrect == 1 & (p14_stereo_1fe$orCol <= 4 | p14_stereo_1fe$orCol >= 9)))
p14_stereo_2fe_numDecFalse_bgl <- length(which(p14_stereo_2fe$oddDecisionCorrect == 1 & p14_stereo_1fe$orCol <= 4))
p14_stereo_2fe_numDecFalse_bgr <- length(which(p14_stereo_2fe$oddDecisionCorrect == 1 & p14_stereo_1fe$orCol >= 9))

p14_stereo_1fe_numDecFalse_all <- length(which(p14_stereo_1fe$oddDecisionCorrect == 1 & p14_stereo_1fe$orCol >= 1 & p14_stereo_1fe$orCol <= 12))
p14_stereo_1fe_numDecFalse_left <- length(which(p14_stereo_1fe$oddDecisionCorrect == 1 & p14_stereo_1fe$orCol <= 4))
p14_stereo_1fe_numDecFalse_center <- length(which(p14_stereo_1fe$oddDecisionCorrect == 1 & p14_stereo_1fe$orCol > 4 & p14_stereo_1fe$orCol < 9))
p14_stereo_1fe_numDecFalse_right <- length(which(p14_stereo_1fe$oddDecisionCorrect == 1 & p14_stereo_1fe$orCol >= 9))

p15_stereo_1fe_numDecFalse <- length(which(p15_stereo_1fe$oddDecisionCorrect == 1))
p15_stereo_2fe_numDecFalse <- length(which(p15_stereo_2fe$oddDecisionCorrect == 1))

p15_stereo_2fe_numDecFalse_fg <- length(which(p15_stereo_2fe$oddDecisionCorrect == 1 & p15_stereo_1fe$orCol > 4 & p15_stereo_1fe$orCol < 9))
p15_stereo_2fe_numDecFalse_bg <- length(which(p15_stereo_2fe$oddDecisionCorrect == 1 & (p15_stereo_1fe$orCol <= 4 | p15_stereo_1fe$orCol >= 9)))
p15_stereo_2fe_numDecFalse_bgl <- length(which(p15_stereo_2fe$oddDecisionCorrect == 1 & p15_stereo_1fe$orCol <= 4))
p15_stereo_2fe_numDecFalse_bgr <- length(which(p15_stereo_2fe$oddDecisionCorrect == 1 & p15_stereo_1fe$orCol >= 9))

p15_stereo_1fe_numDecFalse_all <- length(which(p15_stereo_1fe$oddDecisionCorrect == 1 & p15_stereo_1fe$orCol >= 1 & p15_stereo_1fe$orCol <= 12))
p15_stereo_1fe_numDecFalse_left <- length(which(p15_stereo_1fe$oddDecisionCorrect == 1 & p15_stereo_1fe$orCol <= 4))
p15_stereo_1fe_numDecFalse_center <- length(which(p15_stereo_1fe$oddDecisionCorrect == 1 & p15_stereo_1fe$orCol > 4 & p15_stereo_1fe$orCol < 9))
p15_stereo_1fe_numDecFalse_right <- length(which(p15_stereo_1fe$oddDecisionCorrect == 1 & p15_stereo_1fe$orCol >= 9))

p16_stereo_1fe_numDecFalse <- length(which(p16_stereo_1fe$oddDecisionCorrect == 1))
p16_stereo_2fe_numDecFalse <- length(which(p16_stereo_2fe$oddDecisionCorrect == 1))

p16_stereo_2fe_numDecFalse_fg <- length(which(p16_stereo_2fe$oddDecisionCorrect == 1 & p16_stereo_1fe$orCol > 4 & p16_stereo_1fe$orCol < 9))
p16_stereo_2fe_numDecFalse_bg <- length(which(p16_stereo_2fe$oddDecisionCorrect == 1 & (p16_stereo_1fe$orCol <= 4 | p16_stereo_1fe$orCol >= 9)))
p16_stereo_2fe_numDecFalse_bgl <- length(which(p16_stereo_2fe$oddDecisionCorrect == 1 & p16_stereo_1fe$orCol <= 4))
p16_stereo_2fe_numDecFalse_bgr <- length(which(p16_stereo_2fe$oddDecisionCorrect == 1 & p16_stereo_1fe$orCol >= 9))

p16_stereo_1fe_numDecFalse_all <- length(which(p16_stereo_1fe$oddDecisionCorrect == 1 & p16_stereo_1fe$orCol >= 1 & p16_stereo_1fe$orCol <= 12))
p16_stereo_1fe_numDecFalse_left <- length(which(p16_stereo_1fe$oddDecisionCorrect == 1 & p16_stereo_1fe$orCol <= 4))
p16_stereo_1fe_numDecFalse_center <- length(which(p16_stereo_1fe$oddDecisionCorrect == 1 & p16_stereo_1fe$orCol > 4 & p16_stereo_1fe$orCol < 9))
p16_stereo_1fe_numDecFalse_right <- length(which(p16_stereo_1fe$oddDecisionCorrect == 1 & p16_stereo_1fe$orCol >= 9))

p17_stereo_1fe_numDecFalse <- length(which(p17_stereo_1fe$oddDecisionCorrect == 1))
p17_stereo_2fe_numDecFalse <- length(which(p17_stereo_2fe$oddDecisionCorrect == 1))

p17_stereo_2fe_numDecFalse_fg <- length(which(p17_stereo_2fe$oddDecisionCorrect == 1 & p17_stereo_1fe$orCol > 4 & p17_stereo_1fe$orCol < 9))
p17_stereo_2fe_numDecFalse_bg <- length(which(p17_stereo_2fe$oddDecisionCorrect == 1 & (p17_stereo_1fe$orCol <= 4 | p17_stereo_1fe$orCol >= 9)))
p17_stereo_2fe_numDecFalse_bgl <- length(which(p17_stereo_2fe$oddDecisionCorrect == 1 & p17_stereo_1fe$orCol <= 4))
p17_stereo_2fe_numDecFalse_bgr <- length(which(p17_stereo_2fe$oddDecisionCorrect == 1 & p17_stereo_1fe$orCol >= 9))

p17_stereo_1fe_numDecFalse_all <- length(which(p17_stereo_1fe$oddDecisionCorrect == 1 & p17_stereo_1fe$orCol >= 1 & p17_stereo_1fe$orCol <= 12))
p17_stereo_1fe_numDecFalse_left <- length(which(p17_stereo_1fe$oddDecisionCorrect == 1 & p17_stereo_1fe$orCol <= 4))
p17_stereo_1fe_numDecFalse_center <- length(which(p17_stereo_1fe$oddDecisionCorrect == 1 & p17_stereo_1fe$orCol > 4 & p17_stereo_1fe$orCol < 9))
p17_stereo_1fe_numDecFalse_right <- length(which(p17_stereo_1fe$oddDecisionCorrect == 1 & p17_stereo_1fe$orCol >= 9))

p18_stereo_1fe_numDecFalse <- length(which(p18_stereo_1fe$oddDecisionCorrect == 1))
p18_stereo_2fe_numDecFalse <- length(which(p18_stereo_2fe$oddDecisionCorrect == 1))

p18_stereo_2fe_numDecFalse_fg <- length(which(p18_stereo_2fe$oddDecisionCorrect == 1 & p18_stereo_1fe$orCol > 4 & p18_stereo_1fe$orCol < 9))
p18_stereo_2fe_numDecFalse_bg <- length(which(p18_stereo_2fe$oddDecisionCorrect == 1 & (p18_stereo_1fe$orCol <= 4 | p18_stereo_1fe$orCol >= 9)))
p18_stereo_2fe_numDecFalse_bgl <- length(which(p18_stereo_2fe$oddDecisionCorrect == 1 & p18_stereo_1fe$orCol <= 4))
p18_stereo_2fe_numDecFalse_bgr <- length(which(p18_stereo_2fe$oddDecisionCorrect == 1 & p18_stereo_1fe$orCol >= 9))

p18_stereo_1fe_numDecFalse_all <- length(which(p18_stereo_1fe$oddDecisionCorrect == 1 & p18_stereo_1fe$orCol >= 1 & p18_stereo_1fe$orCol <= 12))
p18_stereo_1fe_numDecFalse_left <- length(which(p18_stereo_1fe$oddDecisionCorrect == 1 & p18_stereo_1fe$orCol <= 4))
p18_stereo_1fe_numDecFalse_center <- length(which(p18_stereo_1fe$oddDecisionCorrect == 1 & p18_stereo_1fe$orCol > 4 & p18_stereo_1fe$orCol < 9))
p18_stereo_1fe_numDecFalse_right <- length(which(p18_stereo_1fe$oddDecisionCorrect == 1 & p18_stereo_1fe$orCol >= 9))

p19_stereo_1fe_numDecFalse <- length(which(p19_stereo_1fe$oddDecisionCorrect == 1))
p19_stereo_2fe_numDecFalse <- length(which(p19_stereo_2fe$oddDecisionCorrect == 1))

p19_stereo_2fe_numDecFalse_fg <- length(which(p19_stereo_2fe$oddDecisionCorrect == 1 & p19_stereo_1fe$orCol > 4 & p19_stereo_1fe$orCol < 9))
p19_stereo_2fe_numDecFalse_bg <- length(which(p19_stereo_2fe$oddDecisionCorrect == 1 & (p19_stereo_1fe$orCol <= 4 | p19_stereo_1fe$orCol >= 9)))
p19_stereo_2fe_numDecFalse_bgl <- length(which(p19_stereo_2fe$oddDecisionCorrect == 1 & p19_stereo_1fe$orCol <= 4))
p19_stereo_2fe_numDecFalse_bgr <- length(which(p19_stereo_2fe$oddDecisionCorrect == 1 & p19_stereo_1fe$orCol >= 9))

p19_stereo_1fe_numDecFalse_all <- length(which(p19_stereo_1fe$oddDecisionCorrect == 1 & p19_stereo_1fe$orCol >= 1 & p19_stereo_1fe$orCol <= 12))
p19_stereo_1fe_numDecFalse_left <- length(which(p19_stereo_1fe$oddDecisionCorrect == 1 & p19_stereo_1fe$orCol <= 4))
p19_stereo_1fe_numDecFalse_center <- length(which(p19_stereo_1fe$oddDecisionCorrect == 1 & p19_stereo_1fe$orCol > 4 & p19_stereo_1fe$orCol < 9))
p19_stereo_1fe_numDecFalse_right <- length(which(p19_stereo_1fe$oddDecisionCorrect == 1 & p19_stereo_1fe$orCol >= 9))

p20_stereo_1fe_numDecFalse <- length(which(p20_stereo_1fe$oddDecisionCorrect == 1))
p20_stereo_2fe_numDecFalse <- length(which(p20_stereo_2fe$oddDecisionCorrect == 1))

p20_stereo_2fe_numDecFalse_fg <- length(which(p20_stereo_2fe$oddDecisionCorrect == 1 & p20_stereo_1fe$orCol > 4 & p20_stereo_1fe$orCol < 9))
p20_stereo_2fe_numDecFalse_bg <- length(which(p20_stereo_2fe$oddDecisionCorrect == 1 & (p20_stereo_1fe$orCol <= 4 | p20_stereo_1fe$orCol >= 9)))
p20_stereo_2fe_numDecFalse_bgl <- length(which(p20_stereo_2fe$oddDecisionCorrect == 1 & p20_stereo_1fe$orCol <= 4))
p20_stereo_2fe_numDecFalse_bgr <- length(which(p20_stereo_2fe$oddDecisionCorrect == 1 & p20_stereo_1fe$orCol >= 9))

p20_stereo_1fe_numDecFalse_all <- length(which(p20_stereo_1fe$oddDecisionCorrect == 1 & p20_stereo_1fe$orCol >= 1 & p20_stereo_1fe$orCol <= 12))
p20_stereo_1fe_numDecFalse_left <- length(which(p20_stereo_1fe$oddDecisionCorrect == 1 & p20_stereo_1fe$orCol <= 4))
p20_stereo_1fe_numDecFalse_center <- length(which(p20_stereo_1fe$oddDecisionCorrect == 1 & p20_stereo_1fe$orCol > 4 & p20_stereo_1fe$orCol < 9))
p20_stereo_1fe_numDecFalse_right <- length(which(p20_stereo_1fe$oddDecisionCorrect == 1 & p20_stereo_1fe$orCol >= 9))

p21_stereo_1fe_numDecFalse <- length(which(p21_stereo_1fe$oddDecisionCorrect == 1))
p21_stereo_2fe_numDecFalse <- length(which(p21_stereo_2fe$oddDecisionCorrect == 1))

p21_stereo_2fe_numDecFalse_fg <- length(which(p21_stereo_2fe$oddDecisionCorrect == 1 & p21_stereo_1fe$orCol > 4 & p21_stereo_1fe$orCol < 9))
p21_stereo_2fe_numDecFalse_bg <- length(which(p21_stereo_2fe$oddDecisionCorrect == 1 & (p21_stereo_1fe$orCol <= 4 | p21_stereo_1fe$orCol >= 9)))
p21_stereo_2fe_numDecFalse_bgl <- length(which(p21_stereo_2fe$oddDecisionCorrect == 1 & p21_stereo_1fe$orCol <= 4))
p21_stereo_2fe_numDecFalse_bgr <- length(which(p21_stereo_2fe$oddDecisionCorrect == 1 & p21_stereo_1fe$orCol >= 9))

p21_stereo_1fe_numDecFalse_all <- length(which(p21_stereo_1fe$oddDecisionCorrect == 1 & p21_stereo_1fe$orCol >= 1 & p21_stereo_1fe$orCol <= 12))
p21_stereo_1fe_numDecFalse_left <- length(which(p21_stereo_1fe$oddDecisionCorrect == 1 & p21_stereo_1fe$orCol <= 4))
p21_stereo_1fe_numDecFalse_center <- length(which(p21_stereo_1fe$oddDecisionCorrect == 1 & p21_stereo_1fe$orCol > 4 & p21_stereo_1fe$orCol < 9))
p21_stereo_1fe_numDecFalse_right <- length(which(p21_stereo_1fe$oddDecisionCorrect == 1 & p21_stereo_1fe$orCol >= 9))

p22_stereo_1fe_numDecFalse <- length(which(p22_stereo_1fe$oddDecisionCorrect == 1))
p22_stereo_2fe_numDecFalse <- length(which(p22_stereo_2fe$oddDecisionCorrect == 1))

p22_stereo_2fe_numDecFalse_fg <- length(which(p22_stereo_2fe$oddDecisionCorrect == 1 & p22_stereo_1fe$orCol > 4 & p22_stereo_1fe$orCol < 9))
p22_stereo_2fe_numDecFalse_bg <- length(which(p22_stereo_2fe$oddDecisionCorrect == 1 & (p22_stereo_1fe$orCol <= 4 | p22_stereo_1fe$orCol >= 9)))
p22_stereo_2fe_numDecFalse_bgl <- length(which(p22_stereo_2fe$oddDecisionCorrect == 1 & p22_stereo_1fe$orCol <= 4))
p22_stereo_2fe_numDecFalse_bgr <- length(which(p22_stereo_2fe$oddDecisionCorrect == 1 & p22_stereo_1fe$orCol >= 9))

p22_stereo_1fe_numDecFalse_all <- length(which(p22_stereo_1fe$oddDecisionCorrect == 1 & p22_stereo_1fe$orCol >= 1 & p22_stereo_1fe$orCol <= 12))
p22_stereo_1fe_numDecFalse_left <- length(which(p22_stereo_1fe$oddDecisionCorrect == 1 & p22_stereo_1fe$orCol <= 4))
p22_stereo_1fe_numDecFalse_center <- length(which(p22_stereo_1fe$oddDecisionCorrect == 1 & p22_stereo_1fe$orCol > 4 & p22_stereo_1fe$orCol < 9))
p22_stereo_1fe_numDecFalse_right <- length(which(p22_stereo_1fe$oddDecisionCorrect == 1 & p22_stereo_1fe$orCol >= 9))

p23_stereo_1fe_numDecFalse <- length(which(p23_stereo_1fe$oddDecisionCorrect == 1))
p23_stereo_2fe_numDecFalse <- length(which(p23_stereo_2fe$oddDecisionCorrect == 1))

p23_stereo_2fe_numDecFalse_fg <- length(which(p23_stereo_2fe$oddDecisionCorrect == 1 & p23_stereo_1fe$orCol > 4 & p23_stereo_1fe$orCol < 9))
p23_stereo_2fe_numDecFalse_bg <- length(which(p23_stereo_2fe$oddDecisionCorrect == 1 & (p23_stereo_1fe$orCol <= 4 | p23_stereo_1fe$orCol >= 9)))
p23_stereo_2fe_numDecFalse_bgl <- length(which(p23_stereo_2fe$oddDecisionCorrect == 1 & p23_stereo_1fe$orCol <= 4))
p23_stereo_2fe_numDecFalse_bgr <- length(which(p23_stereo_2fe$oddDecisionCorrect == 1 & p23_stereo_1fe$orCol >= 9))

p23_stereo_1fe_numDecFalse_all <- length(which(p23_stereo_1fe$oddDecisionCorrect == 1 & p23_stereo_1fe$orCol >= 1 & p23_stereo_1fe$orCol <= 12))
p23_stereo_1fe_numDecFalse_left <- length(which(p23_stereo_1fe$oddDecisionCorrect == 1 & p23_stereo_1fe$orCol <= 4))
p23_stereo_1fe_numDecFalse_center <- length(which(p23_stereo_1fe$oddDecisionCorrect == 1 & p23_stereo_1fe$orCol > 4 & p23_stereo_1fe$orCol < 9))
p23_stereo_1fe_numDecFalse_right <- length(which(p23_stereo_1fe$oddDecisionCorrect == 1 & p23_stereo_1fe$orCol >= 9))

p24_stereo_1fe_numDecFalse <- length(which(p24_stereo_1fe$oddDecisionCorrect == 1))
p24_stereo_2fe_numDecFalse <- length(which(p24_stereo_2fe$oddDecisionCorrect == 1))

p24_stereo_2fe_numDecFalse_fg <- length(which(p24_stereo_2fe$oddDecisionCorrect == 1 & p24_stereo_1fe$orCol > 4 & p24_stereo_1fe$orCol < 9))
p24_stereo_2fe_numDecFalse_bg <- length(which(p24_stereo_2fe$oddDecisionCorrect == 1 & (p24_stereo_1fe$orCol <= 4 | p24_stereo_1fe$orCol >= 9)))
p24_stereo_2fe_numDecFalse_bgl <- length(which(p24_stereo_2fe$oddDecisionCorrect == 1 & p24_stereo_1fe$orCol <= 4))
p24_stereo_2fe_numDecFalse_bgr <- length(which(p24_stereo_2fe$oddDecisionCorrect == 1 & p24_stereo_1fe$orCol >= 9))

p24_stereo_1fe_numDecFalse_all <- length(which(p24_stereo_1fe$oddDecisionCorrect == 1 & p24_stereo_1fe$orCol >= 1 & p24_stereo_1fe$orCol <= 12))
p24_stereo_1fe_numDecFalse_left <- length(which(p24_stereo_1fe$oddDecisionCorrect == 1 & p24_stereo_1fe$orCol <= 4))
p24_stereo_1fe_numDecFalse_center <- length(which(p24_stereo_1fe$oddDecisionCorrect == 1 & p24_stereo_1fe$orCol > 4 & p24_stereo_1fe$orCol < 9))
p24_stereo_1fe_numDecFalse_right <- length(which(p24_stereo_1fe$oddDecisionCorrect == 1 & p24_stereo_1fe$orCol >= 9))


### .. until part 24


# for overall error
all_numDecFalse_1fe <- c(p01_stereo_1fe_numDecFalse, p02_stereo_1fe_numDecFalse, p03_stereo_1fe_numDecFalse, p04_stereo_1fe_numDecFalse, p05_stereo_1fe_numDecFalse, p06_stereo_1fe_numDecFalse, p07_stereo_1fe_numDecFalse, p08_stereo_1fe_numDecFalse, p09_stereo_1fe_numDecFalse, p10_stereo_1fe_numDecFalse, p11_stereo_1fe_numDecFalse, p12_stereo_1fe_numDecFalse, p13_stereo_1fe_numDecFalse, p14_stereo_1fe_numDecFalse, p15_stereo_1fe_numDecFalse, p16_stereo_1fe_numDecFalse, p17_stereo_1fe_numDecFalse, p18_stereo_1fe_numDecFalse, p19_stereo_1fe_numDecFalse, p20_stereo_1fe_numDecFalse, p21_stereo_1fe_numDecFalse, p22_stereo_1fe_numDecFalse, p23_stereo_1fe_numDecFalse, p24_stereo_1fe_numDecFalse)
all_numDecFalse_2fe <- c(p01_stereo_2fe_numDecFalse, p02_stereo_2fe_numDecFalse, p03_stereo_2fe_numDecFalse, p04_stereo_2fe_numDecFalse, p05_stereo_2fe_numDecFalse, p06_stereo_2fe_numDecFalse, p07_stereo_2fe_numDecFalse, p08_stereo_2fe_numDecFalse, p09_stereo_2fe_numDecFalse, p10_stereo_2fe_numDecFalse, p11_stereo_2fe_numDecFalse, p12_stereo_2fe_numDecFalse, p13_stereo_2fe_numDecFalse, p14_stereo_2fe_numDecFalse, p15_stereo_2fe_numDecFalse, p16_stereo_2fe_numDecFalse, p17_stereo_2fe_numDecFalse, p18_stereo_2fe_numDecFalse, p19_stereo_2fe_numDecFalse, p20_stereo_2fe_numDecFalse, p21_stereo_2fe_numDecFalse, p22_stereo_2fe_numDecFalse, p23_stereo_2fe_numDecFalse, p24_stereo_2fe_numDecFalse)

all_rateDecFalse_1fe<- all_numDecFalse_1fe / numTrials
all_rateDecFalse_2fe<- all_numDecFalse_2fe / numTrials

# further data for all participants, TCT, ...

# for all data, spec. duration
all_stereo_1fe <- rbind(p01_stereo_1fe, p02_stereo_1fe, p03_stereo_1fe, p04_stereo_1fe, p05_stereo_1fe, p06_stereo_1fe, p07_stereo_1fe, p08_stereo_1fe, p09_stereo_1fe, p10_stereo_1fe, p11_stereo_1fe, p12_stereo_1fe, p13_stereo_1fe, p14_stereo_1fe, p15_stereo_1fe, p16_stereo_1fe, p17_stereo_1fe, p18_stereo_1fe, p19_stereo_1fe, p20_stereo_1fe, p21_stereo_1fe, p22_stereo_1fe, p23_stereo_1fe, p24_stereo_1fe)
all_stereo_2fe <- rbind(p01_stereo_2fe, p02_stereo_2fe, p03_stereo_2fe, p04_stereo_2fe, p05_stereo_2fe, p06_stereo_2fe, p07_stereo_2fe, p08_stereo_2fe, p09_stereo_2fe, p10_stereo_2fe, p11_stereo_2fe, p12_stereo_2fe, p13_stereo_2fe, p14_stereo_2fe, p15_stereo_2fe, p16_stereo_2fe, p17_stereo_2fe, p18_stereo_2fe, p19_stereo_2fe, p20_stereo_2fe, p21_stereo_2fe, p22_stereo_2fe, p23_stereo_2fe, p24_stereo_2fe)

# for all date error on 1fe
all_numDecFalse_2fe_fg <- c(p01_stereo_2fe_numDecFalse_fg, p02_stereo_2fe_numDecFalse_fg, p03_stereo_2fe_numDecFalse_fg, p04_stereo_2fe_numDecFalse_fg, p05_stereo_2fe_numDecFalse_fg, p06_stereo_2fe_numDecFalse_fg, p07_stereo_2fe_numDecFalse_fg, p08_stereo_2fe_numDecFalse_fg, p09_stereo_2fe_numDecFalse_fg, p10_stereo_2fe_numDecFalse_fg, p11_stereo_2fe_numDecFalse_fg, p12_stereo_2fe_numDecFalse_fg, p13_stereo_2fe_numDecFalse_fg, p14_stereo_2fe_numDecFalse_fg, p15_stereo_2fe_numDecFalse_fg, p16_stereo_2fe_numDecFalse_fg, p17_stereo_2fe_numDecFalse_fg, p18_stereo_2fe_numDecFalse_fg, p19_stereo_2fe_numDecFalse_fg, p20_stereo_2fe_numDecFalse_fg, p21_stereo_2fe_numDecFalse_fg, p22_stereo_2fe_numDecFalse_fg, p23_stereo_2fe_numDecFalse_fg, p24_stereo_2fe_numDecFalse_fg)
all_numDecFalse_2fe_bg <- c(p01_stereo_2fe_numDecFalse_bg, p02_stereo_2fe_numDecFalse_bg, p03_stereo_2fe_numDecFalse_bg, p04_stereo_2fe_numDecFalse_bg, p05_stereo_2fe_numDecFalse_bg, p06_stereo_2fe_numDecFalse_bg, p07_stereo_2fe_numDecFalse_bg, p08_stereo_2fe_numDecFalse_bg, p09_stereo_2fe_numDecFalse_bg, p10_stereo_2fe_numDecFalse_bg, p11_stereo_2fe_numDecFalse_bg, p12_stereo_2fe_numDecFalse_bg, p13_stereo_2fe_numDecFalse_bg, p14_stereo_2fe_numDecFalse_bg, p15_stereo_2fe_numDecFalse_bg, p16_stereo_2fe_numDecFalse_bg, p17_stereo_2fe_numDecFalse_bg, p18_stereo_2fe_numDecFalse_bg, p19_stereo_2fe_numDecFalse_bg, p20_stereo_2fe_numDecFalse_bg, p21_stereo_2fe_numDecFalse_bg, p22_stereo_2fe_numDecFalse_bg, p23_stereo_2fe_numDecFalse_bg, p24_stereo_2fe_numDecFalse_bg)
all_numDecFalse_2fe_bgl <- c(p01_stereo_2fe_numDecFalse_bgl, p02_stereo_2fe_numDecFalse_bgl, p03_stereo_2fe_numDecFalse_bgl, p04_stereo_2fe_numDecFalse_bgl, p05_stereo_2fe_numDecFalse_bgl, p06_stereo_2fe_numDecFalse_bgl, p07_stereo_2fe_numDecFalse_bgl, p08_stereo_2fe_numDecFalse_bgl, p09_stereo_2fe_numDecFalse_bgl, p10_stereo_2fe_numDecFalse_bgl, p11_stereo_2fe_numDecFalse_bgl, p12_stereo_2fe_numDecFalse_bgl, p13_stereo_2fe_numDecFalse_bgl, p14_stereo_2fe_numDecFalse_bgl, p15_stereo_2fe_numDecFalse_bgl, p16_stereo_2fe_numDecFalse_bgl, p17_stereo_2fe_numDecFalse_bgl, p18_stereo_2fe_numDecFalse_bgl, p19_stereo_2fe_numDecFalse_bgl, p20_stereo_2fe_numDecFalse_bgl, p21_stereo_2fe_numDecFalse_bgl, p22_stereo_2fe_numDecFalse_bgl, p23_stereo_2fe_numDecFalse_bgl, p24_stereo_2fe_numDecFalse_bgl)
all_numDecFalse_2fe_bgr <- c(p01_stereo_2fe_numDecFalse_bgr, p02_stereo_2fe_numDecFalse_bgr, p03_stereo_2fe_numDecFalse_bgr, p04_stereo_2fe_numDecFalse_bgr, p05_stereo_2fe_numDecFalse_bgr, p06_stereo_2fe_numDecFalse_bgr, p07_stereo_2fe_numDecFalse_bgr, p08_stereo_2fe_numDecFalse_bgr, p09_stereo_2fe_numDecFalse_bgr, p10_stereo_2fe_numDecFalse_bgr, p11_stereo_2fe_numDecFalse_bgr, p12_stereo_2fe_numDecFalse_bgr, p13_stereo_2fe_numDecFalse_bgr, p14_stereo_2fe_numDecFalse_bgr, p15_stereo_2fe_numDecFalse_bgr, p16_stereo_2fe_numDecFalse_bgr, p17_stereo_2fe_numDecFalse_bgr, p18_stereo_2fe_numDecFalse_bgr, p19_stereo_2fe_numDecFalse_bgr, p20_stereo_2fe_numDecFalse_bgr, p21_stereo_2fe_numDecFalse_bgr, p22_stereo_2fe_numDecFalse_bgr, p23_stereo_2fe_numDecFalse_bgr, p24_stereo_2fe_numDecFalse_bgr)

all_numDecFalse_1fe_left <- c(p01_stereo_1fe_numDecFalse_left, p02_stereo_1fe_numDecFalse_left, p03_stereo_1fe_numDecFalse_left, p04_stereo_1fe_numDecFalse_left, p05_stereo_1fe_numDecFalse_left, p06_stereo_1fe_numDecFalse_left, p07_stereo_1fe_numDecFalse_left, p08_stereo_1fe_numDecFalse_left, p09_stereo_1fe_numDecFalse_left, p10_stereo_1fe_numDecFalse_left, p11_stereo_1fe_numDecFalse_left, p12_stereo_1fe_numDecFalse_left, p13_stereo_1fe_numDecFalse_left, p14_stereo_1fe_numDecFalse_left, p15_stereo_1fe_numDecFalse_left, p16_stereo_1fe_numDecFalse_left, p17_stereo_1fe_numDecFalse_left, p18_stereo_1fe_numDecFalse_left, p19_stereo_1fe_numDecFalse_left, p20_stereo_1fe_numDecFalse_left, p21_stereo_1fe_numDecFalse_left, p22_stereo_1fe_numDecFalse_left, p23_stereo_1fe_numDecFalse_left, p24_stereo_1fe_numDecFalse_left)
all_numDecFalse_1fe_center <- c(p01_stereo_1fe_numDecFalse_center, p02_stereo_1fe_numDecFalse_center, p03_stereo_1fe_numDecFalse_center, p04_stereo_1fe_numDecFalse_center, p05_stereo_1fe_numDecFalse_center, p06_stereo_1fe_numDecFalse_center, p07_stereo_1fe_numDecFalse_center, p08_stereo_1fe_numDecFalse_center, p09_stereo_1fe_numDecFalse_center, p10_stereo_1fe_numDecFalse_center, p11_stereo_1fe_numDecFalse_center, p12_stereo_1fe_numDecFalse_center, p13_stereo_1fe_numDecFalse_center, p14_stereo_1fe_numDecFalse_center, p15_stereo_1fe_numDecFalse_center, p16_stereo_1fe_numDecFalse_center, p17_stereo_1fe_numDecFalse_center, p18_stereo_1fe_numDecFalse_center, p19_stereo_1fe_numDecFalse_center, p20_stereo_1fe_numDecFalse_center, p21_stereo_1fe_numDecFalse_center, p22_stereo_1fe_numDecFalse_center, p23_stereo_1fe_numDecFalse_center, p24_stereo_1fe_numDecFalse_center)
all_numDecFalse_1fe_right <- c(p01_stereo_1fe_numDecFalse_right, p02_stereo_1fe_numDecFalse_right, p03_stereo_1fe_numDecFalse_right, p04_stereo_1fe_numDecFalse_right, p05_stereo_1fe_numDecFalse_right, p06_stereo_1fe_numDecFalse_right, p07_stereo_1fe_numDecFalse_right, p08_stereo_1fe_numDecFalse_right, p09_stereo_1fe_numDecFalse_right, p10_stereo_1fe_numDecFalse_right, p11_stereo_1fe_numDecFalse_right, p12_stereo_1fe_numDecFalse_right, p13_stereo_1fe_numDecFalse_right, p14_stereo_1fe_numDecFalse_right, p15_stereo_1fe_numDecFalse_right, p16_stereo_1fe_numDecFalse_right, p17_stereo_1fe_numDecFalse_right, p18_stereo_1fe_numDecFalse_right, p19_stereo_1fe_numDecFalse_right, p20_stereo_1fe_numDecFalse_right, p21_stereo_1fe_numDecFalse_right, p22_stereo_1fe_numDecFalse_right, p23_stereo_1fe_numDecFalse_right, p24_stereo_1fe_numDecFalse_right)

boxplot(all_numDecFalse_2fe_fg, all_numDecFalse_2fe_bg)
boxplot(all_numDecFalse_2fe_fg, all_numDecFalse_2fe_bg, names=c('projection', 'HMD'), ylab="number of errors", col=c("limegreen", "lightblue"))

all_rateDecFalse_2fe_fg <- all_numDecFalse_2fe_fg / 478
all_rateDecFalse_2fe_bg <- all_numDecFalse_2fe_bg / 478
all_rateDecFalse_2fe_bgl <- all_numDecFalse_2fe_bgl / 478
all_rateDecFalse_2fe_bgr <- all_numDecFalse_2fe_bgr / 478

all_rateDecFalse_1fe_left <- all_numDecFalse_1fe_left / 221
all_rateDecFalse_1fe_center <- all_numDecFalse_1fe_center / 221
all_rateDecFalse_1fe_right <- all_numDecFalse_1fe_right / 221

boxplot(all_rateDecFalse_1fe_left, all_rateDecFalse_2fe_bgl)
boxplot(all_rateDecFalse_1fe_center, all_rateDecFalse_2fe_fg)
boxplot(all_rateDecFalse_1fe_right, all_rateDecFalse_2fe_bgr)
boxplot(all_rateDecFalse_1fe_left*100, all_rateDecFalse_2fe_bgl*100, all_rateDecFalse_1fe_center*100, all_rateDecFalse_2fe_fg*100, all_rateDecFalse_1fe_right*100, all_rateDecFalse_2fe_bgr*100, names=c('HMD left', 'HMD+proj left', 'HMD center', 'HMD+proj center', 'HMD right', 'HMD+proj right'), ylab="error rate (%)", col=c("limegreen", "lightblue"))

boxplot(all_numDecFalse_1fe_left, all_numDecFalse_2fe_bgl, all_numDecFalse_1fe_center, all_numDecFalse_2fe_fg, all_numDecFalse_1fe_right, all_numDecFalse_2fe_bgr, names=c("HMD left", "HMD+proj left", "HMD center", "HMD+proj center", "HMD right", "HMD+proj right"), ylab="number of errors", col=c("limegreen", "lightblue"))

boxplot(all_numDecFalse_1fe_left, all_numDecFalse_2fe_bgl, all_numDecFalse_1fe_center, all_numDecFalse_2fe_fg, all_numDecFalse_1fe_right, all_numDecFalse_2fe_bgr, col=c("limegreen", "lightblue"))
axis(side=1, at=c(1:6), labels=c("HMD left", "HMD+proj left", "HMD center", "HMD+proj center", "HMD right", "HMD+proj right"))


boxplot(all_rateDecFalse_2fe_fg, all_numDecFalse_2fe_bg)
boxplot(all_rateDecFalse_2fe_fg, all_numDecFalse_2fe_bg, names=c('projection', 'HMD'), ylab="error rate (%)", col=c("limegreen", "lightblue"))


#all_stereo_1fe_foreground <- which(all_stereo_1fe$oddDecisionCorrect == 1)
# without outlier
all_stereo_1fe_dur_no_out <- removeOutlierIQR(all_stereo_1fe$duration)
all_stereo_2fe_dur_no_out <- removeOutlierIQR(all_stereo_2fe$duration)

# descriptive statistics
# for tct
summary(all_stereo_1fe$duration)
summary(all_stereo_2fe$duration)

boxplot(all_stereo_1fe$duration, all_stereo_2fe$duration)
boxplot(all_stereo_1fe_dur_no_out, all_stereo_2fe_dur_no_out, names=c('HMD', 'HMD + projection'), ylab="reaction time (ms)", col=c("limegreen", "lightblue"))

# for error
summary(all_numDecFalse_1fe)
summary(all_numDecFalse_2fe)

summary(all_rateDecFalse_1fe)
summary(all_rateDecFalse_2fe)

boxplot(all_numDecFalse_1fe, all_numDecFalse_2fe)
boxplot(all_rateDecFalse_1fe*100, all_rateDecFalse_2fe*100, names=c('HMD', 'HMD + projection'), ylab="error rate (%)", col=c("limegreen", "lightblue"))

# NHST statistical tests
## for duration
# test for normality
shapiro.test(all_stereo_1fe$duration)
shapiro.test(all_stereo_2fe$duration)

shapiro.test(all_stereo_1fe_dur_no_out)
shapiro.test(all_stereo_2fe_dur_no_out)

# --> not normal distributed

# if yes, then t-test
#t.test(all_stereo_1fe$duration, all_stereo_2fe$duration, paired = TRUE)

#if no, the wilcoxon
wilcox.test(all_stereo_1fe$duration, all_stereo_2fe$duration, paired = TRUE, alternative="less", conf.int=TRUE)
wilcoxPairedBatt(all_stereo_1fe$duration, all_stereo_2fe$duration, "less") 
pearsonRToCohenD(-0.335)

# without outlier --> no pairing possible
wilcox.test(all_stereo_1fe_dur_no_out, all_stereo_2fe_dur_no_out, paired = FALSE, alternative="less", conf.int=TRUE)
wilcoxUnpairedBatt(all_stereo_1fe_dur_no_out, all_stereo_2fe_dur_no_out, "less") 

## for error
# test for normality
shapiro.test(all_numDecFalse_1fe)
shapiro.test(all_numDecFalse_2fe)


# if yes, then t-test
#t.test(all_numDecFalse_1fe, all_numDecFalse_2fe, paired = TRUE)

#if no, the wilcoxon
wilcox.test(all_numDecFalse_1fe, all_numDecFalse_2fe, paired = TRUE, alternative="two.sided", conf.int=TRUE)
wilcoxPairedBatt(all_numDecFalse_1fe, all_numDecFalse_2fe, "less") 
wilcoxPairedBatt(all_rateDecFalse_1fe, all_rateDecFalse_2fe, "less") 
pearsonRToCohenD(-0.456)
#--> significant difference

# error left
wilcox.test(all_numDecFalse_1fe_left, all_numDecFalse_2fe_bgl, paired = TRUE, alternative="two.sided", conf.int=TRUE)
wilcoxPairedBatt(all_numDecFalse_1fe_left, all_numDecFalse_2fe_bgl, "less") 
wilcoxPairedBatt(all_numDecFalse_1fe_left, all_numDecFalse_2fe_bgl, "less") 
pearsonRToCohenD(-0.456)

# error center
wilcox.test(all_numDecFalse_1fe_center, all_numDecFalse_2fe_fg, paired = TRUE, alternative="two.sided", conf.int=TRUE)
wilcoxPairedBatt(all_numDecFalse_1fe_center, all_numDecFalse_2fe_fg, "less") 
wilcoxPairedBatt(all_numDecFalse_1fe_center, all_numDecFalse_2fe_fg, "less") 
pearsonRToCohenD(-0.456)

# error right
wilcox.test(all_numDecFalse_1fe_right, all_numDecFalse_2fe_bgl, paired = TRUE, alternative="two.sided", conf.int=TRUE)
wilcoxPairedBatt(all_numDecFalse_1fe_right, all_numDecFalse_2fe_bgl, "less") 
wilcoxPairedBatt(all_numDecFalse_1fe_right, all_numDecFalse_2fe_bgl, "less") 
pearsonRToCohenD(-0.456)



### functions

removeOutlierIQR <- function(vec) {
    result <- vec[!vec %in% boxplot.stats(vec)$out]
    return (result)
}

wilcoxPairedBatt <- function(x, y, alt)  {
	library(coin)
	
	wt1 <- wilcox.test(x, y, alternative = alt, paired=TRUE, conf.int=TRUE)
	print(wt1)
	wt2 <- wilcoxsign_test(x ~ y, exact=TRUE)
	print(wt2)
	# compute pearson r
	zv <- statistic(wt2)
	n <- length(x) + length(y)
	r <- zv/sqrt(n)
	print("Pearson's r");
	print(r)
}

wilcoxUnpairedBatt <- function(x, y, alt)  {
	library(coin)
	
	wt1 <- wilcox.test(x, y, alternative = alt, paired=FALSE, conf.int=TRUE)
	print(wt1)
	
	obs <- c(x, y)
	groups <- c(rep("x", length(x)), rep("y", length(y)))
	
	tmpDF <- data.frame(obs, groups)
	colnames(tmpDF) <- c("obs", "groups")
	
	wt2 <- wilcox_test(obs ~ groups, data = tmpDF, exact=TRUE)
	#wt2 <- wilcox_test(x ~ y, exact=TRUE)
	print(wt2)
	# compute pearson r
	zv <- statistic(wt2)
	n <- length(x) + length(y)
	r <- zv/sqrt(n)
	print("Pearson's r");
	print(r)
}

ttestPairedBatt <- function(x,y, alt) {
	tRes <- t.test(x,y, alternative = alt, paired=TRUE)
	
	# effect ssize (cohen's d)
	# see http://yatani.jp/HCIstats/TTest
	sdDiff <- sd(x -  y)
	meanDiff <- tRes$estimate # str(tres)
	cohensD <- abs(meanDiff) / sdDiff
	
	print(tRes)
	print("Cohen's d:")
	print(cohensD)
}

ttestUnpairedBatt <- function(x,y, alt) {
	tRes <- t.test(x,y, alternative = alt, paired=FALSE)
	
	#http://yatani.jp/HCIstats/TTest#EffectSizeUnaired
	cohensD <- cohensDTTestUnpairedGroups(x,y)
	
	print(tRes)
	print("Cohen's d:")
	print(cohensD)

}

pearsonRToCohenD <- function(r) {
	return ((2*r) / (sqrt(1-r*r)))
}



/////// NUR DIESE VARIABLEN ÄNDERN START ///////

var maxNumTrials = 104.0; // 256.0 // WICHTIG: zahl mit .0 angeben und sollte vielfaches von acht sein
// we assume a random number in the range of [0..1]
// the following variables are the max cummulative thresholds for certain events


var _maxChanceOdd = 1.0; // we have a 50% chance of showing an odd ring
var _maxChanceOddCenter = 0.0; // if we show an odd ring, we have a 50% chance of showing it in the center --> total chance of 25%


/*
// VORTEST_VERSCH.GRÖßEN_145%
var _baseNameOneFLNoOdd = "/Matrizen/Vortest_versch.Groeßen/145_Prozent/one_focuslayer/no_odd/";
var _baseNameOneFLOdd = "/Matrizen/Vortest_versch.Groeßen/145_Prozent/one_focuslayer/odd/";

var _baseNameTwoFLProj = "/Matrizen/Vortest_versch.Groeßen/145_Prozent/two_focuslayer/projection/";

var _baseNameTwoFLHMDNoOdd = "/Matrizen/Vortest_versch.Groeßen/145_Prozent/two_focuslayer/hmd/no_odd/";
var _baseNameTwoFLHMDOdd = "/Matrizen/Vortest_versch.Groeßen/145_Prozent/two_focuslayer/hmd/odd/";
*/


 //VORTEST_VERSCH.GRÖßEN_100%
var _baseNameOneFLNoOdd = "/Matrizen/one_focuslayer/no_odd/";
var _baseNameOneFLOdd = "/Matrizen/one_focuslayer/odd/";

var _baseNameTwoFLProj = "/Matrizen/two_focuslayer/projection/";

var _baseNameTwoFLHMDNoOdd = "/Matrizen/two_focuslayer/hmd/no_odd/";
var _baseNameTwoFLHMDOdd = "/Matrizen/two_focuslayer/hmd/odd/";


/*
 //VORTEST_VERSCH.GRÖßEN_55%
var _baseNameOneFLNoOdd = "/Matrizen/Vortest_versch.Groeßen/55_Prozent/one_focuslayer/no_odd/";
var _baseNameOneFLOdd = "/Matrizen/Vortest_versch.Groeßen/55_Prozent/one_focuslayer/odd/";

var _baseNameTwoFLProj = "/Matrizen/Vortest_versch.Groeßen/55_Prozent/two_focuslayer/projection/";

var _baseNameTwoFLHMDNoOdd = "/Matrizen/Vortest_versch.Groeßen/55_Prozent/two_focuslayer/hmd/no_odd/";
var _baseNameTwoFLHMDOdd = "/Matrizen/Vortest_versch.Groeßen/55_Prozent/two_focuslayer/hmd/odd/";
 */




/////// NUR DIESE VARIABLEN ÄNDERN ENDE ///////

/////// AB HIER NICHTS MEHR ÄNDERN ///////

var http = require("http")
var express = require('express')
var app = express()
var url = require('url')
var fs = require('fs')
//var conf = require('./config.json');


// web server
// http und WebSocket Modul laden
var server = require('http').  createServer()


//Server auf Port 4080 starten
var port = 4080;
server.listen(port);

  //, function() {console.log('Listening on ' 
//+ server.address().port) });


// Server-Anfragen akzeptieren
server.on('request', app);

//Verknüpfung mit HTML-Datei
app.use(express.static(__dirname + '/public'));


//Teil-Matrix
app.get('/hmd_vorphase/', function (req, res) {
  //Datei index.html ausgeben
  res.sendFile(__dirname + '/public//HMD_vorphase.html');
});


//Teil-Matrix
app.get('/projektion_vorphase/', function (req, res) {
  //Datei index.html ausgeben
  res.sendFile(__dirname + '/public/Projektion_vorphase.html');
});


// Kontroll-Webseite
app.get('/control_vorphase/', function (req, res) {
  //Datei index.html ausgeben
  res.sendFile(__dirname + '/public/control_vorphase.html');
});

var _curCondition = 0; // should be "1" for 1 focus layer and "2" for two focus layers
var _curTrialStartTime = 0;
var _curTrialEndTime = 0;
var _curTrial = 0;
var _curForegroundDiffers = 0;

//var curNumOddForeground = 0;
var maxNumOddForeground = 8;

var _curBGOrientation = "";
var _curOROrientation = "";
var _curORPosition = "";

var _startPosOR = "";

var _curCorrectDecision = "";

// we want to make sure that every odd position in the periphery is exactly shown the same number of times as the other positions
var maxNumOddPerPeripheryPosition = (maxNumTrials * _maxChanceOdd * (1.0 - _maxChanceOddCenter));


console.log("maxNumOddPerPeripheryPosition: " + maxNumOddPerPeripheryPosition);
var _curNumORPositions = [0];

/*
var _curNumOddPOL = 0; var _curNumOddPO = 0;
var _curNumOddPOR = 0; var _curNumOddPL = 0;
var _curNumOddPR = 0; var _curNumOddPUL = 0;
var _curNumOddPU = 0; var _curNumOddPUR = 0;
 */

function nextTrial() {
    _curBGOrientation = "r";//bgOrientations[Math.round(Math.random()*(bgOrientations.length-1))];
    _curOROrientation = orOrientations[Math.round(Math.random()*(orOrientations.length-1))];
    //_curORPosition = "or";

    _curORPosition = _startPosOR;
            
   
    if(_curCondition == "1") {    
        var imageNameHMD = _baseNameOneFLOdd + _curORPosition + "_" + _curOROrientation + "_"+ _curBGOrientation + ".png";     
        var imageNameProj = "null";
        var data = '{"action": "nextTrial", "source": "server_vorphase", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';
        wss.broadcast(data);
    } else if(_curCondition == "2") {    
        var imageNameHMD = _baseNameTwoFLHMDOdd + _curORPosition + "_" + _curOROrientation + "_"+ _curBGOrientation + ".png";     
        var imageNameProj = _baseNameTwoFLProj + _curBGOrientation + ".png"; 
        var data = '{"action": "nextTrial", "source": "server_vorphase", "fileHMD": "' + imageNameHMD + '", "fileProj": "' + imageNameProj + '"}';
        wss.broadcast(data);
    }
             
             

    
   _curTrialStartTime = new Date().getTime();
}
// websocket
// https://github.com/websockets/ws
var WebSocketServer = require('ws').Server;
 
 wss = new WebSocketServer({ port: 4081 });
 //wss = new WebSocketServer(wsServerURL);


var trialRunning = false;

var bgOrientations = ["ol", "o", "or", "r", "ur", "u", "ul", "l"];
//var bgOrientations = ["r","r"];
//var orOrientations = ["ol", "o", "or", "r", "ur", "u", "ul", "l"];
var orOrientations = ["ol", "o", "or", "r", "ur", "u", "ul", "l"];

//FÜR VORTEST: 9 orPositions
//var orPositions = [1, 2, 3, 4, 5, 6, 7, 8, 9];

//FÜR HAUPTEST: 9 orPositions
//
//var orPositions = [1, 2, 3, 4, 5, 6, 7, 8];


//var orPositions = ["ol", "ol"];

var orPositions = ["ol", "o", "or", "l", "r", "ul", "u", "ur"];

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
   // 
      
    var jo = JSON.parse(message);
            
    if(jo.source == "control_vorphase") {
     
        
        if(jo.action == "startCondition" && trialRunning == false) {
           console.log("startCondition");   
            trialRunning = true;
           
           // create log file
            createLogFile(jo.id, jo.condition)
            console.log("_curFileName: " + _curFileName);
           
            logToFile("trialNum;oddDecisionCorrect;duration;bgOrientation;orPosition;orOrientation;\n");
            // choose background orientation, odd ring orientation, odd ring position
            /*bgOrientations.shuffle();
            orOrientations.shuffle();
            orPositions.shuffle();
            */
            // set the current condition:
            _curCondition = jo.condition;
            _startPosOR = jo.orpos;
            nextTrial();
            _curTrial++;
            
       }
    }
      
    
    
      
    if(jo.source == "projection_vorphase") {
        
       if(jo.action == "ul") {
           console.log("ul");   
           
           // inform control about user decision
           
           // log decision
           
           _curTrialEndTime = new Date().getTime();
           var duration = _curTrialEndTime - _curTrialStartTime;
           
           var decisionCorrect = 0;
           if(_curOROrientation == "ul") {
               decisionCorrect = 1;
               var data = '{"action": "beepRight", "source": "server_vorphase"}';
               wss.broadcast(data);   
           } else {
               var data = '{"action": "beepError", "source": "server_vorphase"}';
               wss.broadcast(data);   
           }
        
          logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curBGOrientation + ";" + _curORPosition + ";" + _curOROrientation + "\n" );
           
           // show next image
           if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server_vorphase"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }
       } // end if action "ul"

        else if(jo.action == "u") {
           console.log("u");   
           
           _curTrialEndTime = new Date().getTime();
           var duration = _curTrialEndTime - _curTrialStartTime;
           
            var decisionCorrect = 0;
           if(_curOROrientation == "u") {
               decisionCorrect = 1;
               var data = '{"action": "beepRight", "source": "server_vorphase"}';
               wss.broadcast(data);   
           } else {
               var data = '{"action": "beepError", "source": "server_vorphase"}';
               wss.broadcast(data);   
           }
        
          logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curBGOrientation + ";" + _curORPosition + ";" + _curOROrientation + "\n" );
           
           // show next image
           if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server_vorphase"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }
       } // end if action "u"
        else if(jo.action == "ur") {
           console.log("ur");  
           
           _curTrialEndTime = new Date().getTime();
           var duration = _curTrialEndTime - _curTrialStartTime;
           
            var decisionCorrect = 0;
           if(_curOROrientation == "ur") {
               decisionCorrect = 1;
               var data = '{"action": "beepRight", "source": "server_vorphase"}';
               wss.broadcast(data);   
           } else {
               var data = '{"action": "beepError", "source": "server_vorphase"}';
               wss.broadcast(data);   
           }
        
          logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curBGOrientation + ";" + _curORPosition + ";" + _curOROrientation + "\n" );
           
           // show next image
           if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server_vorphase"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }
       } // end if action "ur"

           else if(jo.action == "l") {
           console.log("l");  
           
           _curTrialEndTime = new Date().getTime();
           var duration = _curTrialEndTime - _curTrialStartTime;
           
            var decisionCorrect = 0;
           if(_curOROrientation == "l") {
               decisionCorrect = 1;
               var data = '{"action": "beepRight", "source": "server_vorphase"}';
               wss.broadcast(data);   
           } else {
               var data = '{"action": "beepError", "source": "server_vorphase"}';
               wss.broadcast(data);   
           }
        
          logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curBGOrientation + ";" + _curORPosition + ";" + _curOROrientation + "\n" );
           
           // show next image
           if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server_vorphase"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }
       } // end if action "l"

           else if(jo.action == "r") {
           console.log("r");  
           
           _curTrialEndTime = new Date().getTime();
           var duration = _curTrialEndTime - _curTrialStartTime;
           
            var decisionCorrect = 0;
           if(_curOROrientation == "r") {
               decisionCorrect = 1;
               var data = '{"action": "beepRight", "source": "server_vorphase"}';
               wss.broadcast(data);   
           } else {
               var data = '{"action": "beepError", "source": "server_vorphase"}';
               wss.broadcast(data);   
           }
        
          logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curBGOrientation + ";" + _curORPosition + ";" + _curOROrientation + "\n" );
           
           // show next image
           if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server_vorphase"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }
       } // end if action "r"

           else if(jo.action == "ol") {
           console.log("ol");  
           
           _curTrialEndTime = new Date().getTime();
           var duration = _curTrialEndTime - _curTrialStartTime;
           
            var decisionCorrect = 0;
           if(_curOROrientation == "ol") {
               decisionCorrect = 1;
               var data = '{"action": "beepRight", "source": "server_vorphase"}';
               wss.broadcast(data);   
           } else {
               var data = '{"action": "beepError", "source": "server_vorphase"}';
               wss.broadcast(data);   
           }
        
          logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curBGOrientation + ";" + _curORPosition + ";" + _curOROrientation + "\n" );
           
           // show next image
           if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server_vorphase"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }
       } // end if action "ol"

           else if(jo.action == "o") {
           console.log("o");  
           
           _curTrialEndTime = new Date().getTime();
           var duration = _curTrialEndTime - _curTrialStartTime;
           
            var decisionCorrect = 0;
           if(_curOROrientation == "o") {
               decisionCorrect = 1;
               var data = '{"action": "beepRight", "source": "server_vorphase"}';
               wss.broadcast(data);   
           } else {
               var data = '{"action": "beepError", "source": "server_vorphase"}';
               wss.broadcast(data);   
           }
        
          logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curBGOrientation + ";" + _curORPosition + ";" + _curOROrientation + "\n" );
           
           // show next image
           if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server_vorphase"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }
       } // end if action "o"

           else if(jo.action == "or") {
           console.log("or");  
           
           _curTrialEndTime = new Date().getTime();
           var duration = _curTrialEndTime - _curTrialStartTime;
           
            var decisionCorrect = 0;
           if(_curOROrientation == "or") {
               decisionCorrect = 1;
               var data = '{"action": "beepRight", "source": "server_vorphase"}';
               wss.broadcast(data);   
           } else {
               var data = '{"action": "beepError", "source": "server_vorphase"}';
               wss.broadcast(data);   
           }
        
          logToFile(_curTrial + ";"  + decisionCorrect + ";" + duration + ";" + _curBGOrientation + ";" + _curORPosition + ";" + _curOROrientation + "\n" );
           
           // show next image
           if(_curTrial < maxNumTrials) {
               nextTrial();
               _curTrial++;
           } else {
               var data = '{"action": "end", "source": "server_vorphase"}';
               wss.broadcast(data);   
               console.log("condition has ended");   
           }
       } // end if action "or"
    }  
  });

//  ws.send('something');
});


// SERVER SENDING BROADCAST DATA
wss.broadcast = function broadcast(data) {
  wss.clients.forEach(function each(client) {
  client.send(data);
  });
};


// helper functions
// zufällige Anzeige der Matrizenbilder
Array.prototype.shuffle = function() {
    var input = this; 
    for (var i = input.length-1; i >=0; i--) {     
        var randomIndex = Math.floor(Math.random()*(i+1)); 
        var itemAtIndex = input[randomIndex]; 

        input[randomIndex] = input[i]; 
        input[i] = itemAtIndex;
    } return input;
} // end function


// logging
var _curFileName = "";
function createLogFile(participant, condition) {
    _curFileName = "logs_vorphase/" + _curFileName + new Date().getTime() + "_" + participant + "_" + condition + ".csv";
}
function logToFile(strToLog) {
 fs.appendFile (_curFileName, strToLog, function(err, fd) {
   if (err) {
       return console.error(err);
   }
  console.log("Appended successfully!");     
});   
}

// Portnummer in die Konsole schreiben
console.log('Der Server läuft nun unter http://127.0.0.1:' + port + '/');


